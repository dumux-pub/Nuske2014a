// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   Copyright (C) 2009 by Andreas Lauser
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Components
 * \brief A very simple (everything constant) version of the component Fluorinert 43. Fluorinert is a trademark
 *        of the company 3M. It is a family fluorocarbon-based liquids that can be engineered
 *        to suit the needs of (mostly) cooling applications such as
 *        boiling point etc.
 *
 *        It is inert and has low heat conductivity.
 */
#ifndef DUMUX_SIMPLE_FLUORINERT43_HH
#define DUMUX_SIMPLE_FLUORINERT43_HH

#include <dumux/material/idealgas.hh>

#include <cmath>

namespace Dumux
{
/*!
 * \ingroup Components
 *
 * \brief A very simple (everything constant) version of the component Fluorinert. Fluorinert is a trademark
 *        of the company 3M. It is a family of fluorocarbon-based liquids that can be engineered
 *        to suit the needs of (mostly) cooling applications such as
 *        boiling point etc.
 *
 *        It is inert and has low heat conductivity.
 *
 *        source: company product information
 *        http://solutions.3m.com/wps/portal/3M/en_US/ElectronicsChemicals/Home/Products/ProductCatalog/?PC_7_RJH9U5230001F0I2QNAAFE3GR3000000_nid=4JN4Z87P3Zbe0QZ9T6GGF9gl
 *
 *        all values at 25°C
 *
 * \tparam Scalar The type used for scalar values
 */
template <class Scalar>
class SimpleFluorinert43 : public Component<Scalar, SimpleFluorinert43<Scalar> >
{
    typedef Dumux::IdealGas<Scalar> IdealGas;

    static const Scalar R;  // specific gas constant of Fluorinert 43

public:
    /*!
     * \brief A human readable name for the water.
     */
    static const char *name()
    { return "Fluorinert"; }

    /*!
     * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of Fluorinert 43.
     */
    static Scalar molarMass()
    { return 670e-3 ; /* [kg/mol] */ }

    /*!
     * \brief Returns the critical temperature \f$\mathrm{[K]}\f$ of Fluorinert 43.
     */
    static Scalar criticalTemperature()
    { return 567.; /* [K] */ }

    /*!
     * \brief Returns the critical pressure \f$\mathrm{[Pa]}\f$ of Fluorinert 43.
     */
    static Scalar criticalPressure()
    { return 1.13e6; /* [Pa] */ }

    /*!
     * \brief Returns the temperature \f$\mathrm{[K]}\f$ at the triple point.
     */
    static Scalar tripleTemperature(){
        DUNE_THROW(Dune::NotImplemented,
            "No value for triple temperature implemented."); /* [K] */ }

    /*!
     * \brief Returns the pressure \f$\mathrm{[Pa]}\f$ at the triple point.
     */
    static Scalar triplePressure(){
        DUNE_THROW(Dune::NotImplemented,
                "No value for triple pressure implemented.");/* [Pa] */  }

    /*!
     * \brief The vapor pressure in \f$\mathrm{[Pa]}\f$ of pure fluorinert
     *        at a given temperature.
     *
     *\param T temperature of component in \f$\mathrm{[K]}\f$
     *
     */
    static Scalar vaporPressure(Scalar T)
    { return 192.; /*[Pa]*/ }

    /*!
     * \brief Specific liquid heat capacity of Fluorinert 43 \f$\mathrm{[J/kg K]}\f$.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidHeatCapacity(const Scalar temperature, const Scalar pressure)
    {
    	return 1100.; // J/(kg K)
    }

    /*!
     * \brief Specific enthalpy of Fluorinert 43 \f$\mathrm{[J/kg]}\f$.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static const Scalar liquidEnthalpy(const Scalar temperature,
                                       const Scalar pressure)
    {
    	const Scalar cp = liquidHeatCapacity(temperature, pressure);
    	return cp * (temperature - 273.15); /*[J/kg]*/ }

    /*!
     * \brief Specific enthalpy of gaseous Fluorinert 43 \f$\mathrm{[J/kg]}\f$.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static const Scalar gasEnthalpy(Scalar temperature,
                                    Scalar pressure)
    { DUNE_THROW(Dune::NotImplemented,
            "No value for gas enthalpy implemented.");/* [J/kg] */ }

    /*!
     * \brief Specific internal energy of steam \f$\mathrm{[J/kg]}\f$.
     *
     *        Definition of enthalpy: \f$h= u + pv = u + p / \rho\f$.
     *
     *        Rearranging for internal energy yields: \f$u = h - pv\f$.
     *
     *        Exploiting the Ideal Gas assumption (\f$pv = R_{\textnormal{specific}} T\f$)gives: \f$u = h - R / M T \f$.
     *
     *        The universal gas constant can only be used in the case of molar formulations.
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static const Scalar gasInternalEnergy(Scalar temperature,
                                          Scalar pressure)
    {
        return
            gasEnthalpy(temperature, pressure) -
            1/molarMass()* // conversion from [J/(mol K)] to [J/(kg K)]
            IdealGas::R*temperature; // = pressure *spec. volume for an ideal gas
    }

    /*!
     * \brief Specific internal energy of liquid Fluorinert 43\f$\mathrm{[J/kg]}\f$.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static const Scalar liquidInternalEnergy(Scalar temperature,
                                             Scalar pressure)
    {
        return
            liquidEnthalpy(temperature, pressure) -
            pressure/liquidDensity(temperature, pressure);
    }

    /*!
     * \brief Returns true iff the gas phase is assumed to be compressible
     */
    static bool gasIsCompressible()
    { return true; }

    /*!
     * \brief Returns true iff the liquid phase is assumed to be compressible
     */
    static bool liquidIsCompressible()
    { return false; }

    /*!
     * \brief The density \f$\mathrm{[kg/m^3]}\f$ of gaseous Fluorinert 43 at a given pressure and temperature.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar gasDensity(Scalar temperature, Scalar pressure)
    {
        // Assume an ideal gas
        return molarMass()*IdealGas::molarDensity(temperature, pressure);
    }

    /*!
     * \brief Returns true iff the gas phase is assumed to be ideal
     */
    static bool gasIsIdeal()
    { return true; }

    /*!
     * \brief The pressure of gaseous Fluorinert 43in \f$\mathrm{[Pa]}\f$ at a given density and temperature.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param density density of component in \f$\mathrm{[kg/m^3]}\f$
     */
    static Scalar gasPressure(Scalar temperature, Scalar density)
    {
        // Assume an ideal gas
        return IdealGas::pressure(temperature, density/molarMass());
    }

    /*!
     * \brief The density of pure Fluorinert 43 at a given pressure and temperature \f$\mathrm{[kg/m^3]}\f$.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidDensity(Scalar temperature, Scalar pressure)
    {
        return 1860. /*[kg/m^3]*/ ;
    }

    /*!
     * \brief The pressure of Fluorinert 43 in \f$\mathrm{[Pa]}\f$ at a given density and temperature.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param density density of component in \f$\mathrm{[kg/m^3]}\f$
     */
    static Scalar liquidPressure(Scalar temperature, Scalar density)
    {
        DUNE_THROW(Dune::InvalidStateException,
                   "The liquid pressure is undefined for incompressible fluids");
    }

    /*!
     * \brief The dynamic viscosity \f$\mathrm{[Pa*s]}\f$ of gaseous Fluorinert 43.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     * \param regularize defines, if the functions is regularized or not, set to true by default
     */
    static Scalar gasViscosity(Scalar temperature, Scalar pressure, bool regularize=true){
        DUNE_THROW(Dune::NotImplemented,
            "No value for gas viscosity implemented."); /* [Pa s] */ }

    /*!
     * \brief The dynamic viscosity \f$\mathrm{[Pa*s]}\f$ of pure Fluorinert 43.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidViscosity(Scalar temperature, Scalar pressure)
    { return 4.7e-3;/* [Pa s] */ }

};

template <class Scalar>
const Scalar SimpleFluorinert43<Scalar>::R = Dumux::Constants<Scalar>::R / molarMass();

} // end namespace

#endif
