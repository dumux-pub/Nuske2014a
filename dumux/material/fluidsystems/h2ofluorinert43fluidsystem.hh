// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A twophase fluid system with water and fluorinert as phases.
 */
#ifndef DUMUX_H2O_FLUORINERT_FLUID_SYSTEM_HH
#define DUMUX_H2O_FLUORINERT_FLUID_SYSTEM_HH

#include <dumux/material/idealgas.hh>

#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/components/simplefluorinert43.hh>
#include <dumux/material/components/tabulatedcomponent.hh>

#include <dumux/common/valgrind.hh>
#include <dumux/common/exceptions.hh>

#include <dumux/material/fluidsystems/basefluidsystem.hh>

#include <assert.h>

#ifdef DUMUX_PROPERTIES_HH
#include <dumux/common/basicproperties.hh>
#include <dumux/material/fluidsystems/defaultcomponents.hh>
#endif

namespace Dumux
{
namespace FluidSystems
{

/*!
 * \ingroup Fluidsystems
 *
 * \brief A twophase fluid system with water and fluorinert as components.
 *        BEWARE in this case water is non-wetting and Fluorinert is wetting
 *
 * This FluidSystem can be used without the PropertySystem that is applied in Dumux,
 * as all Parameters are defined via template parameters. Hence it is in an
 * additional namespace Dumux::FluidSystem::.
 * An adapter class using Dumux::FluidSystem<TypeTag> is also provided
 * at the end of this file.
 */
template <class Scalar, bool enableDiffusion>
class H2OFluorinert43
    : public BaseFluidSystem< Scalar ,
                              H2OFluorinert43<Scalar, enableDiffusion> >
{
        static_assert( ! (enableDiffusion) ,
                "There are no diffusion coefficients implemented in this fluidsystem."
                "Therefore, this fluidsystem cannot be used with diffusion.");

    typedef H2OFluorinert43<Scalar, enableDiffusion> ThisType;
    typedef BaseFluidSystem<Scalar, ThisType> Base;

    // convenience typedefs
    typedef Dumux::IdealGas<Scalar> IdealGas;
    typedef Dumux::H2O<Scalar> IapwsH2O;
    typedef Dumux::SimpleH2O<Scalar> SimpleH2O;

    typedef Dumux::TabulatedComponent<Scalar, IapwsH2O > TabulatedH2O;
    typedef Dumux::SimpleFluorinert43<Scalar> Fluorinert;

public:

    typedef Dumux::NullParameterCache ParameterCache;

    /****************************************
     * Fluid phase related static parameters
     ****************************************/

    //! Number of phases in the fluid system
    static constexpr int numPhases = 2;

    //! Index of the water phase, here water is non-wetting
    static const int wPhaseIdx = 0 ;
    //! Index of the non-water phase, here this is fluorinert
    static const int nPhaseIdx = 1 ;
    //! Index of the solid phase
    static const int sPhaseIdx = 2 ;

    // export component indices to indicate the main component
    // of the corresponding phase at atmospheric pressure 1 bar
    // and room temperature 20��C:
    static const int wCompIdx = wPhaseIdx;
    static const int nCompIdx = nPhaseIdx;

    /*!
     * \brief Return the human readable name of a fluid phase
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static const char *phaseName(const unsigned int phaseIdx)
    {
        static const char *name[] = {
            "w",
            "n",
            "s"
        };

        assert(0 <= phaseIdx && phaseIdx < numPhases+1);
        return name[phaseIdx];
    }

    /*!
     * \brief Return whether a phase is liquid
     *        Fluorinert AND water both are liquid for ambient conditions.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isLiquid(const unsigned int phaseIdx)
    {
        return true;
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be an ideal mixture.
     *
     * We define an ideal mixture as a fluid phase where the fugacity
     * coefficients of all components times the pressure of the phase
     * are indepent on the fluid composition. This assumtion is true
     * if Henry's law and Rault's law apply. If you are unsure what
     * this function should return, it is safe to return false. The
     * only damage done will be (slightly) increased computation times
     * in some cases.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isIdealMixture(const unsigned int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        // we assume Henry's and Rault's laws for the water phase and
        // and no interaction between gas molecules of different
        // components, so all phases are ideal mixtures!
        return true;
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be compressible.
     *
     * Compressible means that the partial derivative of the density
     * to the fluid pressure is always larger than zero.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isCompressible(const unsigned int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        // fluorinert has constant values
        if (phaseIdx == wPhaseIdx)
            return false;
        else if (phaseIdx == nPhaseIdx)
            // the water component decides for the liquid phase...
            return H2O::liquidIsCompressible();
        else
            DUNE_THROW(Dune::NotImplemented, "wrong index");
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be an ideal gas.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isIdealGas(const unsigned int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        return false; // water AND fluorinert are liquids
    }

    /****************************************
     * Component related static parameters
     ****************************************/

    //! Number of components in the fluid system
    static const int numComponents = 2;

    //! The components for pure water
//    typedef TabulatedH2O H2O;
    typedef SimpleH2O H2O;
    //typedef IapwsH2O H2O;

    /*!
     * \brief Return the human readable name of a component
     *
     * \param compIdx The index of the component to consider
     */
    static const char *componentName(const unsigned int compIdx)
    {
        static const char *name[] = {
                H2O::name(),
                Fluorinert::name()
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return name[compIdx];
    }

    /*!
     * \brief Return the molar mass of a component in [kg/mol].
     *
     * \param compIdx The index of the component to consider
     */
    static Scalar molarMass(const unsigned int compIdx)
    {
        static const Scalar M[] = {
                H2O::molarMass(),
                Fluorinert::molarMass()
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return M[compIdx];
    }

    /*!
     * \brief Critical temperature of a component [K].
     *
     * \param compIdx The index of the component to consider
     */
    static Scalar criticalTemperature(const unsigned int compIdx)
    {
        static const Scalar Tcrit[] = {
                H2O::criticalTemperature(), // H2O
                Fluorinert::criticalTemperature(), // Fluorinert
        };
        assert(0 <= compIdx && compIdx < numComponents);
        return Tcrit[compIdx];
    }

    /*!
     * \brief Critical pressure of a component [Pa].
     *
     * \param compIdx The index of the component to consider
     */
    static Scalar criticalPressure(const unsigned int compIdx)
    {
        static const Scalar pcrit[] = {
                H2O::criticalPressure(),
                Fluorinert::criticalPressure()
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return pcrit[compIdx];
    }

    /*!
     * \brief Molar volume of a component at the critical point [m^3/mol].
     *
     * \param compIdx The index of the component to consider
     */
    static Scalar criticalMolarVolume(const unsigned int compIdx)
    {
        DUNE_THROW(Dune::NotImplemented,
                   "H2OFluorinertFluidSystem::criticalMolarVolume()");
        return 0.;
    }

    /*!
     * \brief The acentric factor of a component [].
     *
     * \param compIdx The index of the component to consider
     */
    static Scalar acentricFactor(const unsigned int compIdx)
    {
        static const Scalar accFac[] = {
                H2O::acentricFactor(), // H2O (from Reid, et al.)
                Fluorinert::acentricFactor()
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return accFac[compIdx];
    }

    /****************************************
     * thermodynamic relations
     ****************************************/

    /*!
     * \brief Initialize the fluid system's static parameters generically
     *
     * If a tabulated H2O component is used, we do our best to create
     * tables that always work.
     */
    static void init()
    {
        init(/*tempMin=*/273.15,
             /*tempMax=*/623.15,
             /*numTemp=*/100,
             /*pMin=*/0.0,
             /*pMax=*/20e6,
             /*numP=*/200);
    }

    /*!
     * \brief Initialize the fluid system's static parameters using
     *        problem specific temperature and pressure ranges
     *
     * \param tempMin The minimum temperature used for tabulation of water [K]
     * \param tempMax The maximum temperature used for tabulation of water [K]
     * \param nTemp The number of ticks on the temperature axis of the  table of water
     * \param pressMin The minimum pressure used for tabulation of water [Pa]
     * \param pressMax The maximum pressure used for tabulation of water [Pa]
     * \param nPress The number of ticks on the pressure axis of the  table of water
     */
    static void init(Scalar tempMin, Scalar tempMax, unsigned nTemp,
                     Scalar pressMin, Scalar pressMax, unsigned nPress)
    {   std::cout << "Using H2O-Fluorinert fluid system\n";

        if (H2O::isTabulated) {
            std::cout << "Initializing tables for the H2O fluid properties ("
                      << nTemp*nPress
                      << " entries).\n";

            TabulatedH2O::init(tempMin, tempMax, nTemp,
                               pressMin, pressMax, nPress);
        }
    }

    /*!
     * \brief Calculate the density [kg/m^3] of a fluid phase
     *
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     */
    using Base::density;
    template <class FluidState>
    static Scalar density(const FluidState & fluidState,
                             const unsigned int phaseIdx)
    {
        assert(0 <= phaseIdx  && phaseIdx < numPhases);

        Scalar T = fluidState.temperature(phaseIdx);
        Scalar p = fluidState.pressure(phaseIdx);

        // water phase
        if (phaseIdx == wPhaseIdx)
                return H2O::liquidDensity(T, p);
        // non-water phase
        else if (phaseIdx == nPhaseIdx)
            return
                Fluorinert::liquidDensity(T, p);
        else
            DUNE_THROW(Dune::NotImplemented, "wrong index");
    }

    /*!
     * \brief Calculate the dynamic viscosity of a fluid phase [Pa*s]
     *
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     */
    using Base::viscosity;
    template <class FluidState>
    static Scalar viscosity(const FluidState & fluidState,
                            const unsigned int phaseIdx)
    {
        assert(0 <= phaseIdx  && phaseIdx < numPhases);

        Scalar T = fluidState.temperature(phaseIdx);
        Scalar p = fluidState.pressure(phaseIdx);

        // water phase
        if (phaseIdx == wPhaseIdx)
            // assume pure water for the non-wetting phase
            return H2O::liquidViscosity(T, p);

        // non-water phase
        else if (phaseIdx == nPhaseIdx)
            // assume pure fluorinert for the wetting phase
            return Fluorinert::liquidViscosity(T, p);

        else
            DUNE_THROW(Dune::NotImplemented, "wrong index");
    }

    /*!
     * \brief Calculate the fugacity coefficient [Pa] of an individual
     *        component in a fluid phase
     *
     * The fugacity coefficient \f$\phi^\kappa_\alpha\f$ of
     * component \f$\kappa\f$ in phase \f$\alpha\f$ is connected to
     * the fugacity \f$f^\kappa_\alpha\f$ and the component's mole
     * fraction \f$x^\kappa_\alpha\f$ by means of the relation
     *
     * \f[
     f^\kappa_\alpha = \phi^\kappa_\alpha\;x^\kappa_\alpha\;p_\alpha
     \f]
     * where \f$p_\alpha\f$ is the pressure of the fluid phase.
     *
     * The quantity "fugacity" itself is just an other way to express
     * the chemical potential \f$\zeta^\kappa_\alpha\f$ of the
     * component. It is defined via
     *
     * \f[
     f^\kappa_\alpha := \exp\left\{\frac{\zeta^\kappa_\alpha}{k_B T_\alpha} \right\}
     \f]
     * where \f$k_B = 1.380\cdot10^{-23}\;J/K\f$ is the Boltzmann constant.
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     * \param compIdx The index of the component to consider
     *
     * this is an UGLY solution, but now mixing is possible with this fluidsystem anyways.
     */
    using Base::fugacityCoefficient;
    template <class FluidState>
    static Scalar fugacityCoefficient(const FluidState &fluidState,
                                      const unsigned int phaseIdx,
                                      const unsigned int compIdx)
    {
//        DUNE_THROW(Dune::NotImplemented, "fugacityCoefficient");
        #warning RETURNING ZERO FUGACITY COEFFICIENT
        return 0;
    }


    /*!
     * \brief Calculate the molecular diffusion coefficient for a
     *        component in a fluid phase [mol^2 * s / (kg*m^3)]
     *
     * Molecular diffusion of a compoent \f$\kappa\f$ is caused by a
     * gradient of the chemical potential and follows the law
     *
     * \f[ J = - D \mathbf{grad} \mu_\kappa \f]
     *
     * where \f$\mu_\kappa\f$ is the component's chemical potential,
     * \f$D\f$ is the diffusion coefficient and \f$J\f$ is the
     * diffusive flux. \f$mu_\kappa\f$ is connected to the component's
     * fugacity \f$f_\kappa\f$ by the relation
     *
     * \f[ \mu_\kappa = R T_\alpha \mathrm{ln} \frac{f_\kappa}{p_\alpha} \f]
     *
     * where \f$p_\alpha\f$ and \f$T_\alpha\f$ are the fluid phase'
     * pressure and temperature.
     *
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     * \param compIdx The index of the component to consider
     *
     * // UGLY solution but there is anyways no dissolution with this fluidsystem.
     */
    using Base::diffusionCoefficient;
    template <class FluidState>
    static Scalar diffusionCoefficient(const FluidState & fluidState,
                                       const unsigned int phaseIdx,
                                       const unsigned int compIdx)
    {
        DUNE_THROW(Dune::NotImplemented, "diffusion Coefficient");
        return 0;
    }

    /*!
     * \brief Given a phase's composition, temperature and pressure,
     *        return the binary diffusion coefficient for components
     *        \f$i\f$ and \f$j\f$ in this phase.
     *
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     * \param compIIdx The index of the first component to consider
     * \param compJIdx The index of the second component to consider
     *
     * // UGLY solution but there is anyways no compositional effects with this fluidsystem.
     */
    using Base::binaryDiffusionCoefficient;
    template <class FluidState>
    static Scalar binaryDiffusionCoefficient(const FluidState & fluidState,
                                                  const unsigned int phaseIdx,
                                                  const unsigned int compIIdx,
                                                  const unsigned int compJIdx)

    {
#warning RETURNING ZERO DIFFUSION COEFFICIENT
        return 0.;
    }

    /*!
     * \brief Given a phase's composition, temperature, pressure and
     *        density, calculate its specific enthalpy [J/kg].
     *
     *  \todo This fluid system neglects the contribution of
     *        gas-molecules in the liquid phase. This contribution is
     *        probably not big. Somebody would have to find out the
     *        enthalpy of solution for this system. ...
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     */
    using Base::enthalpy;
    template <class FluidState>
    static Scalar enthalpy(const FluidState & fluidState,
                              const unsigned int phaseIdx)
    {
        const Scalar T = fluidState.temperature(phaseIdx);
        const Scalar p = fluidState.pressure(phaseIdx);
        Valgrind::CheckDefined(T);
        Valgrind::CheckDefined(p);

        // water phase
        if (phaseIdx == wPhaseIdx)
            return H2O::liquidEnthalpy(T, p);

        // non-water phase
        else if (phaseIdx == nPhaseIdx)
            return Fluorinert::liquidEnthalpy(T, p);

        else
            DUNE_THROW(Dune::NotImplemented, "wrong index");
    }

    /*!
     * \brief Thermal conductivity of a fluid phase [W/(m K)].
     *
     * Use the conductivity of air and water as a first approximation.
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     */
    using Base::thermalConductivity;
    template <class FluidState>
    static Scalar thermalConductivity(const FluidState &fluidState,
                                          const unsigned int phaseIdx)
    {
        assert(0 <= phaseIdx  && phaseIdx < numPhases);

        // water phase
        if (phaseIdx == wPhaseIdx)
                return  0.578078;   // conductivity of water[W / (m K ) ] IAPWS evaluated at p=.1 MPa, T=8��C
        // non-water phase
        else if (phaseIdx == nPhaseIdx)// wetting phase
            return 0.065; // conductivity of Fluorinert 43 [W / (m K ) ]
//        source: http://solutions.3m.com/wps/portal/3M/en_US/ElectronicsChemicals/Home/Products/ProductCatalog/?PC_7_RJH9U5230001F0I2QNAAFE3GR3000000_nid=4JN4Z87P3ZbeV9PG0RBKJ7gl

        else
            DUNE_THROW(Dune::NotImplemented, "wrong index");
    }

    /*!
     * \brief Specific isobaric heat capacity of a fluid phase.
     *        \f$\mathrm{[J/kg]}\f$.
     *
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     */
    using Base::heatCapacity;
    template <class FluidState>
    static Scalar heatCapacity(const FluidState & fluidState,
                                  const unsigned int phaseIdx)
    {
        const Scalar T = fluidState.temperature(phaseIdx);
        const Scalar p = fluidState.pressure(phaseIdx);

        // water phase
        if (phaseIdx == wPhaseIdx)
            return H2O::liquidHeatCapacity(T,p);

        // non-water phase
        else if (phaseIdx == nPhaseIdx)
            return Fluorinert::liquidHeatCapacity(T,p);

        else
            DUNE_THROW(Dune::NotImplemented, "wrong index");
    }

    /*!
     * \brief Give the Henry constant for a component in a phase.
     *        This is an UGLY solution, but this fluidsystem is to
     *        be used ONLY for the case of immiscibility.
     *
     */
    static Scalar henry(Scalar temperature)
    {
        // no dissolution effects in this fluidsystem
        DUNE_THROW(Dune::NotImplemented, "henry");
        return 0; // Pa
    }

    /*!
     * \brief Give the vapor pressure of a component above one phase.
     *        This is an UGLY solution, but this fluidsystem is to
     *        be used ONLY for the case of immiscibility.
     */
    static Scalar vaporPressure(Scalar temperature)
    {
        // no dissolution effects in this fluidsystem
        DUNE_THROW(Dune::NotImplemented, "vaporPressure");
        return 0; // Pa
    }


    /*!
     * \brief Give the enthalpy of a *component* in a phase.
     */
    template <class FluidState>
    static Scalar componentEnthalpy(FluidState & fluidState,
                                    const unsigned int phaseIdx,
                                    const unsigned int compIdx)
    {
        const Scalar T = fluidState.temperature(phaseIdx);
        const Scalar p = fluidState.pressure(phaseIdx);
        Valgrind::CheckDefined(T);
        Valgrind::CheckDefined(p);
        switch (phaseIdx){
            // non-water phase
            case nPhaseIdx:
                switch(compIdx){
                case nCompIdx:
                    return Fluorinert::liquidEnthalpy(T, p);
                case wCompIdx:
                    return H2O::liquidEnthalpy(T, p);
                default:
                    DUNE_THROW(Dune::NotImplemented,
                               "wrong index");
                    break;
                }// end switch compIdx
                break;
            // water phase
            case wPhaseIdx:
                switch(compIdx){
                   case nCompIdx:
                       return Fluorinert::liquidEnthalpy(T, p);
                   case wCompIdx:
                       return H2O::liquidEnthalpy(T, p);
                   default:
                       DUNE_THROW(Dune::NotImplemented,
                                  "wrong index");
                       break;
                   }// end switch compIdx
                break;
            default:
                DUNE_THROW(Dune::NotImplemented,
                           "wrong index");
                break;
        }// end switch phaseIdx
    }


    /*!
     * \brief Returns the equilibrium mole fraction of a component in the other phase.
     *
     *        I.e. this is a (2-phase) solution of the problem:  I know the composition of a
     *        phase and want to know the composition of the other phase.
     *
     *        \param fluidState A container with the current (physical) state of the fluid
     *        \param paramCache A container for iterative calculation of fluid composition
     *        \param referencePhaseIdx The index of the phase who's composition is known.
     *        \parma calcCompIdx The component who's composition in the other phase is to be
     *               calculated.
     */
    template <class FluidState>
    static void calculateEquilibriumMoleFractionOtherPhase(FluidState & fluidState,
                                                    const ParameterCache & paramCache,
                                                    const unsigned int referencePhaseIdx,
                                                    const unsigned int calcCompIdx)
    {
        assert(0 <= referencePhaseIdx and referencePhaseIdx < numPhases);
        assert(0 <= calcCompIdx and calcCompIdx < numComponents);

        static_assert(( (numComponents==numPhases)  and (numPhases==2) ),
                      "This function requires that the number of fluid phases is equal "
                      "to the number of components");

        // the index of the other phase
        // for the 2-phase case, this is easy: nPhase in, wPhase set   ; wPhase in, nPhase set
        const unsigned int valueOfnPhaseIdx = nPhaseIdx ;
        const unsigned int valueOfwPhaseIdx = wPhaseIdx ;
        const unsigned int otherPhaseIdx = referencePhaseIdx==wPhaseIdx ? valueOfnPhaseIdx : valueOfwPhaseIdx ;

        assert(0 <= referencePhaseIdx and referencePhaseIdx < numPhases);

        // idea: - the mole fraction of a component in a phase is known.
        //       - by means of functional relations, the mole fraction of the
        //         same component in the other phase can be calculated.

        // therefore in: phaseIdx, compIdx, out (into fluidState): mole fraction of compIdx in otherPhaseIdx

        // the switch is based on what is known:
        // the mole fraction of one component in one phase
        switch (referencePhaseIdx)
        {
        // non-water phase
        case  nPhaseIdx  :
            switch (calcCompIdx)
            {
            case wCompIdx :
            {
                const Scalar xww = 1. ;
                Valgrind::CheckDefined(xww);
                fluidState.setMoleFraction(otherPhaseIdx, calcCompIdx, xww) ;
                return;
                break;
            }

            case nCompIdx :
            {
                const Scalar xwn = 0. ;
                Valgrind::CheckDefined(xwn);
                fluidState.setMoleFraction(otherPhaseIdx, calcCompIdx, xwn) ;
                return ;
            }

            default: DUNE_THROW(Dune::NotImplemented, "wrong index");
            break;
            }
            break;

        // water phase
        case wPhaseIdx:
            switch (calcCompIdx)
            {
            case wCompIdx :
            {
                const Scalar xnw = 0. ;
                Valgrind::CheckDefined(xnw);
                fluidState.setMoleFraction(otherPhaseIdx, calcCompIdx, xnw) ;
                return ;
                break ;
            }

            case nCompIdx :
            {
                const Scalar xnn = 1. ;
                Valgrind::CheckDefined(xnn);
                fluidState.setMoleFraction(otherPhaseIdx, calcCompIdx, xnn) ;
                return ;
            }

            default: DUNE_THROW(Dune::NotImplemented, "wrong index");
            break;
            }

            DUNE_THROW(Dune::NotImplemented, "wrong index");
            break;
        }
    }

    /*!
     * \brief Calculates the equilibrium composition for a given temperature and pressure.
     *
     *        In general a system of equations needs to be solved for this. In the case of
     *        a 2 component system, this can done by hand.
     *
     *        However, in this particular case, we assume that the phases are pure and do not mix.
     *
     *        \param fluidState A container with the current (physical) state of the fluid
     *        \param paramCache A container for iterative calculation of fluid composition
     */
    template <class FluidState>
    static void calculateEquilibriumMoleFractions(FluidState & fluidState,
                                                  const ParameterCache & paramCache)
    {
        static_assert(( (numComponents==numPhases)  and (numPhases==2) ),
                      "This function requires that the number fluid phases is equal "
                      "to the number of components");

        Scalar x[numPhases][numComponents] ;

        x[wPhaseIdx][wCompIdx] = 1. ;
        x[wPhaseIdx][nCompIdx] = 0. ;
        x[nPhaseIdx][wCompIdx] = 0. ;
        x[nPhaseIdx][nCompIdx] = 1. ;

        // set the mole fraction in the fluid state
        for(int phaseIdx=0; phaseIdx<numPhases; phaseIdx++){
            for(int compIdx=0; compIdx<numComponents; compIdx++){
                Valgrind::CheckDefined(x[phaseIdx][compIdx]);
                fluidState.setMoleFraction(phaseIdx, compIdx, x[phaseIdx][compIdx]) ;
            }
        }
        return ;
    }



};

} // end namespace FluidSystems

//#ifdef DUMUX_PROPERTIES_HH
///*!
// * \brief A twophase fluid system with water and fluorinert as components.
// *
// * This is an adapter to use Dumux::H2OFluorinertFluidSystem<TypeTag>, as is
// * done with most other classes in Dumux.
// */
//template<class TypeTag>
//class H2OFluorinert43FluidSystem
//: public FluidSystems::H2OFluorinert43<typename GET_PROP_TYPE(TypeTag, Scalar) >
//{};
//#endif

} // end namespace Dumux

#endif
