/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
/*!
 * \file
 * Specification of the material params for the interfacial area surface
 * parameters
 */
#ifndef LINEAR_INTERFACIAL_AREA_PARAMS_HH
#define LINEAR_INTERFACIAL_AREA_PARAMS_HH

namespace Dumux
{
/*!
 * \brief implementation of interfacial area surface params
 */
template<class ScalarT>
class LinearInterfacialAreaParams
{
public:
    typedef ScalarT Scalar;

    LinearInterfacialAreaParams()
    {}

    LinearInterfacialAreaParams(const Scalar a1)
    {
        setA1(a1);
    }

    /*!
         * \brief Return the \f$a_{1}\f$ shape parameter of awn surface.
         */
        const Scalar a1() const
        { return a1_; }

    /*!
     * \brief Set the \f$a_{1}\f$ shape parameter.
     */
    void setA1(const Scalar v)
    { a1_ = v; }


    /*!
     * \brief Set the \f$S_{wr}\f$ for the surface.
     */
    void setSwr(const Scalar v)
    { Swr_ = v; }

    /*!
     * \brief Return the \f$a_{3}\f$ shape parameter of awn surface.
     */
    const Scalar Swr() const
    { return Swr_; }

private:
    Scalar a1_;
    Scalar Swr_;
};
} // namespace Dumux

#endif
