/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
/*!
 * \file 
 * Specification of the material params specific interfacial area surface
 * as a function of wetting phase Saturation and capillary pressure.
 */

#ifndef LINEAR_INTERFACIAL_AREA_HH
#define LINEAR_INTERFACIAL_AREA_HH

#include "linearInterfacialAreaparams.hh"

#include <dune/common/exceptions.hh>


#include <algorithm>

#include <math.h>
#include <assert.h>

namespace Dumux
{
/*!
 * \ingroup material
 *
 * \brief Implementation of the exponential function relating
 *             specific interfacial area to wetting phase saturation and capillary pressure as suggested by Nuske(2009) (Diploma thesis).
 */
template <class ScalarT, class ParamsT =LinearInterfacialAreaParams<ScalarT> >
class LinearInterfacialArea
{
public:
    typedef ParamsT Params;
    typedef typename Params::Scalar Scalar;

    /*!
     * \brief The interfacial area surface
     *
     * the suggested (as estimated from pore network models) awn surface:
     * \f[
        a_{wn} = a_1 * (S_{wr}-S_w) .* (1-S_w) + a_2 * (S_{wr}-S_w) * (1-S_w) * \exp( a_3 * p_c) ;
     \f]
     * \param  params parameter container for the coefficients of the surface
     * \param  Sw Effective saturation of the wetting phase
     * \param  pc Capillary pressure
     */
    static Scalar interfacialArea(const Params & params, const Scalar Sw, const Scalar pc)
    {
        const Scalar Swr = params.Swr();
        const Scalar a1 = params.a1();

        const Scalar aAlphaBeta = a1 * Sw - a1;
        return aAlphaBeta;
    }

    /*! \brief the derivative of specific interfacial area function w.r.t. capillary pressure
     *
     * \param  params parameter container for the coefficients of the surface
     * \param  Sw Effective saturation of the wetting phase
     * \param  pc Capillary pressure
     */
    static Scalar dawn_dpc (const Params & params, const Scalar Sw, const Scalar pc)
    {
        DUNE_THROW(Dune::NotImplemented, __FILE__ << "  dawndpc()");
    }

    /*! \brief the derivative of specific interfacial area function w.r.t. saturation
     *
     * \param  params parameter container for the coefficients of the surface
     * \param  Sw Effective saturation of the wetting phase
     * \param  pc Capillary pressure
     */
    static Scalar dawn_dsw (const Params & params, const Scalar Sw, const Scalar pc)
    {
        DUNE_THROW(Dune::NotImplemented, __FILE__ << "  dawndpc()");
    }

};
} // namespace Dumux

#endif // Guardian


