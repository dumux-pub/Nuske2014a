// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief   Parameters that are necessary for the \em regularization of
 *          VanGenuchten "material law".
 */

#ifndef REGULARIZED_VAN_GENUCHTEN_KRPC_DIFFERTENTPARAMS_PARAMS_HH
#define REGULARIZED_VAN_GENUCHTEN_KRPC_DIFFERTENTPARAMS_PARAMS_HH

#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchtenparams.hh>

namespace Dumux
{
/*!
 *
 *
 * \brief   Parameters that are necessary for the \em regularization of
 *          VanGenuchten "material law".
 *
 * \ingroup fluidmatrixinteractionsparams
 */
template<class ScalarT>
class RegularizedVanGenuchtenParamsKrPcDifferentParms : public RegularizedVanGenuchtenParams<ScalarT>
{
public:
    typedef ScalarT Scalar;
    typedef VanGenuchtenParams<Scalar> Parent;

    RegularizedVanGenuchtenParamsKrPcDifferentParms()
    {}

    RegularizedVanGenuchtenParamsKrPcDifferentParms(Scalar vgAlpha,
                                  Scalar vgN)
        : Parent(vgAlpha, vgN)
    {}

    /*!
     * \brief Return the \f$m\f$ shape parameter [-] of van Genuchten's
     *        curve.
     */
    Scalar vgkrm() const
    { return vgkrm_; }

    /*!
     * \brief Set the \f$m\f$ shape parameter [-] of van Genuchten's
     *        curve.
     *
     * The \f$n\f$ shape parameter is set to \f$n = \frac{1}{1 - m}\f$
     */
    void setVgkrm(Scalar krm)
    { vgkrm_ = krm; vgkrn_ = 1/(1 - vgkrm_); }

    /*!
     * \brief Return the \f$n\f$ shape parameter [-] of van Genuchten's
     *        curve.
     */
    Scalar vgkrn() const
    { return vgkrn_; }

    /*!
     * \brief Set the \f$n\f$ shape parameter [-] of van Genuchten's
     *        curve.
     *
     * The \f$n\f$ shape parameter is set to \f$m = 1 - \frac{1}{n}\f$
     */
    void setVgkrn(Scalar krn)
    { vgkrn_ = krn; vgkrm_ = 1 - 1/vgkrn_; }

private:
    Scalar vgkrm_;
    Scalar vgkrn_;

};
} // namespace Dumux

#endif
