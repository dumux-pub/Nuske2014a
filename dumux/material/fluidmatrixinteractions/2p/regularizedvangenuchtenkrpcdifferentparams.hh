// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Implementation of the regularized version of the van Genuchten's
 *        capillary pressure / relative permeability  <-> saturation relation.
 */
#ifndef REGULARIZED_VAN_GENUCHTEN_KRPC_DIFFERTENTPARAMS_HH
#define REGULARIZED_VAN_GENUCHTEN_KRPC_DIFFERTENTPARAMS_HH

#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh>

#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchtenparamskrpcdifferentparams.hh>

#include <algorithm>


#include <dumux/common/spline.hh>

namespace Dumux
{
/*!\ingroup fluidmatrixinteractionslaws


 * \brief Implementation of the regularized  van Genuchten's
 *        capillary pressure / relative permeability  <-> saturation relation.
 *
 *        This class bundles the "raw" curves as
 *        static members and doesn't concern itself converting
 *        absolute to effective saturations and vice versa.
 *
 *        In order to avoid very steep gradients the marginal values
 *        are "regularized".  This means that in stead of following
 *        the curve of the material law in these regions, some linear
 *        approximation is used.  Doing this is not worse than
 *        following the material law. E.g. for very low wetting phase
 *        values the material laws predict infinite values for
 *        \f$p_c\f$ which is completely unphysical. In case of very
 *        high wetting phase saturations the difference between
 *        regularized and "pure" material law is not big.
 *
 *        Regularizing has the additional benefit of being numerically
 *        friendly: Newton's method does not like infinite gradients.
 *
 *        The implementation is accomplished as follows:
 *        - check whether we are in the range of regularization
 *         - yes: use the regularization
 *         - no: forward to the standard material law.
 *
 *        An example of the regularization of the capillary pressure curve is shown below:
 *        \image html regularizedVanGenuchten.png
 *
 * \see VanGenuchten
 */
template <class ScalarT, class ParamsT = RegularizedVanGenuchtenParamsKrPcDifferentParms<ScalarT> >
class RegularizedVanGenuchtenDifferentKrPc : public RegularizedVanGenuchten<ScalarT, ParamsT>
{

public:
    typedef ParamsT Params;
    typedef typename Params::Scalar Scalar;



    /*!
     * \brief   Regularized version of the  relative permeability
     *          for the wetting phase of
     *          the medium implied by the van Genuchten
     *          parameterization.
     *
     *  regularized part:
     *    - below \f$ \overline S_w =0\f$:                  set relative permeability to zero
     *    - above \f$ \overline S_w =1\f$:                  set relative permeability to one
     *    - between \f$ 0.95 \leq \overline S_w \leq 1\f$:  use a spline as interpolation
     *
     *  For not-regularized part:
        \copydetails VanGenuchten::krw()
     */
    static Scalar krw(const Params &params, Scalar swe)
    {
        // retrieve the high threshold saturation for the
        // unregularized relative permeability curve of the wetting
        // phase from the parameters
        const Scalar swThHigh = params.krwHighSw();

        if (swe < 0)
            return 0;
        else if (swe > 1)
            return 1;
        else if (swe > swThHigh) {
            typedef Dumux::Spline<Scalar> Spline;
            Spline sp(swThHigh, 1.0, // x1, x2
                      krw_(params, swThHigh), 1.0, // y1, y2
                      dkrw_dsw_(params, swThHigh), 0); // m1, m2
            return sp.eval(swe);
        }

        return krw_(params, swe);
    }

    /*!
     * \brief   Regularized version of the  relative permeability
     *          for the non-wetting phase of
     *          the medium implied by the van Genuchten
     *          parameterization.
     *
     * regularized part:
     *    - below \f$ \overline S_w =0\f$:                  set relative permeability to zero
     *    - above \f$ \overline S_w =1\f$:                  set relative permeability to one
     *    - for \f$ 0 \leq \overline S_w \leq 0.05 \f$:     use a spline as interpolation
     *
         \copydetails VanGenuchten::krn()
     *
     */
    static Scalar krn(const Params &params, Scalar swe)
    {
        // retrieve the low threshold saturation for the unregularized
        // relative permeability curve of the non-wetting phase from
        // the parameters
        const Scalar swThLow = params.krnLowSw();

        if (swe <= 0)
            return 1;
        else if (swe >= 1)
            return 0;
        else if (swe < swThLow) {
            typedef Dumux::Spline<Scalar> Spline;
            Spline sp(0.0, swThLow, // x1, x2
                      1.0, krn_(params, swThLow), // y1, y2
                      0.0, dkrn_dsw_(params, swThLow)); // m1, m2
            return sp.eval(swe);
        }

        return krn_(params, swe);
    }

private:
    /*!
     * \brief The relative permeability for the wetting phase of
     *        the medium implied by van Genuchten's
     *        parameterization.
     *
     * \param swe        The mobile saturation of the wetting phase.
     * \param params    A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen, and then the params container
     *                  is constructed accordingly. Afterwards the values are set there, too.     */
    static Scalar krw_(const Params &params, Scalar swe)
    {
        assert(0 <= swe && swe <= 1);

        const Scalar krw_mParamter =  params.vgkrm() ;

        Scalar r = 1.0 - pow(1.0 - pow(swe, 1.0/krw_mParamter), krw_mParamter);
        return sqrt(swe)*r*r;
    }

    /*!
     * \brief The derivative of the relative permeability for the
     *        wetting phase in regard to the wetting saturation of the
     *        medium implied by the van Genuchten parameterization.
     *
     * \param swe       The mobile saturation of the wetting phase.
     * \param params    A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen, and then the params container
     *                  is constructed accordingly. Afterwards the values are set there, too.
     */
    static Scalar dkrw_dsw_(const Params &params, Scalar swe)
    {
        assert(0 <= swe && swe <= 1);

        const Scalar krw_mParamter =  params.vgkrm() ;

        const Scalar x = 1.0 - std::pow(swe, 1.0/krw_mParamter);
        const Scalar xToM = std::pow(x, krw_mParamter);
        return (1.0 - xToM)/std::sqrt(swe) * ( (1.0 - xToM)/2 + 2*xToM*(1.0-x)/x );
    }

    /*!
     * \brief The relative permeability for the non-wetting phase
     *        of the medium implied by van Genuchten's
     *        parameterization.
     *
     * \param swe        The mobile saturation of the wetting phase.
     * \param params    A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen, and then the params container
     *                  is constructed accordingly. Afterwards the values are set there, too.
     */
    static Scalar krn_(const Params &params, Scalar swe)
    {
        assert(0 <= swe && swe <= 1);

        const Scalar krn_mParamter =  params.vgkrm() ;


        return
            pow(1 - swe, 1.0/3) *
            pow(1 - pow(swe, 1.0/krn_mParamter), 2*krn_mParamter);
    }

    /*!
     * \brief The derivative of the relative permeability for the
     *        non-wetting phase in regard to the wetting saturation of
     *        the medium as implied by the van Genuchten
     *        parameterization.
     *
     * \param swe        The mobile saturation of the wetting phase.
     * \param params    A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen, and then the params container
     *                  is constructed accordingly. Afterwards the values are set there, too.
     */
    static Scalar dkrn_dsw_(const Params &params, Scalar swe)
    {
        assert(0 <= swe && swe <= 1);

        const Scalar krn_mParamter =  params.vgkrm() ;

        const Scalar x = std::pow(swe, 1.0/krn_mParamter);
        return
            -std::pow(1.0 - x, 2*krn_mParamter)
            *std::pow(1.0 - swe, -2.0/3)
            *(1.0/3 + 2*x/swe);
    }


};

}

#endif
