/*****************************************************************************
 *   Copyright (C) 2008 by Andreas Lauser                                    *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief A collection of functions for output purposes.
 *          These output files are meant for visualization with another program (matlab, gnuplot...)
 *
 */
#ifndef DUMUX_OUTPUTTOFILE_HH
#define DUMUX_OUTPUTTOFILE_HH

#include <dumux/common/valgrind.hh>
#include <dumux/common/propertysystem.hh>


#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <iostream>
#include <fstream>


namespace Dumux
{
namespace Properties
{
    NEW_PROP_TAG(Scalar);
    NEW_PROP_TAG(Problem);
    NEW_PROP_TAG(GridView);
    NEW_PROP_TAG(DofMapper);
    NEW_PROP_TAG(ElementSolutionVector);
    NEW_PROP_TAG(SolutionVector);
    NEW_PROP_TAG(FVElementGeometry);
    NEW_PROP_TAG(TwoPIAIndices);
    NEW_PROP_TAG(NumEq);
    NEW_PROP_TAG(MaterialLaw);
    NEW_PROP_TAG(ElementVolumeVariables);
    NEW_PROP_TAG(AwnSurface);
    NEW_PROP_TAG(AwnSurfaceParams);
}

template<class TypeTag>
class SmallResultWriter
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar)               Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Problem)              Problem;
    typedef typename GET_PROP_TYPE(TypeTag, GridView)             GridView;
    typedef typename GridView::template Codim<0>::Entity                Element;
    typedef typename GridView::template Codim<0>::Iterator              ElementIterator;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry)    FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, DofMapper)            DofMapper;
    typedef typename GET_PROP_TYPE(TypeTag, ElementSolutionVector) ElementSolutionVector;
    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector)       SolutionVector;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw)          MaterialLaw;
    typedef typename MaterialLaw::Params                                MaterialLawParams;

    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
//    typedef typename GET_PROP_TYPE(TypeTag, MPNCIndices)        Indices;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem)          FluidSystem;



    enum {

        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,
        sPhaseIdx = FluidSystem::sPhaseIdx,

        // Grid and world dimension
        dim         = GridView::dimension,
        dimWorld    = GridView::dimensionworld,
    };
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef typename GridView::template Codim<dim>::Iterator VertexIterator;


public:
    /*
     * \brief A function that writes results over space in columns into a textfile.
     *
     *        The Writer needs to be called in the init() function of the problem, and afterwards
     *        from postTimeStep().
     *
     *        This function puts output variables (TemperaturePhase, Saturation, t, tIndex, ...) over space (1D) into a text file,
     *        so they can be read in by another program like matlab.
     *        The file can be found by the extension: dat
     */
    void write(const Problem & problem)
    {
        static_assert(dim==1, "the results are output over space. So far this is only implemented 1D");

        static bool firstHeaderFlag     = true ; // the first time sth is written to the file, we write a header
        static bool initialFlag   = true ; // time(Idx) are treated differently for the initial solution

        typedef typename GridView::template Codim<0>::Entity Element;
        typedef typename GridView::template Codim<0>::Iterator ElementIterator;
        typedef typename GET_PROP_TYPE(TypeTag, DofMapper) DofMapper;
        typedef typename GET_PROP_TYPE(TypeTag, ElementSolutionVector) ElementSolutionVector;
        typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;

        Scalar time;
        int timeStepIndex;

        FVElementGeometry fvElemGeom;
        ElementVolumeVariables elemVolVars;

        // Looping over all elements of the domain
        ElementIterator endIt = problem.gridView().template end<0>();
        // In order to treat the last element I need to have an iterator, that is
        // always one further.
        ElementIterator pasingElementIt = problem.gridView().template begin<0>();
        ++pasingElementIt;
        for (ElementIterator elementIt = problem.gridView().template begin<0>() ; elementIt != endIt; ++elementIt)
        {
            // updating the volume variables
            fvElemGeom.update(problem.gridView(), *elementIt);
            elemVolVars.update(problem, *elementIt, fvElemGeom, false);

            int scvIdx = 0 ; // because I want to visit each location only once i.e. UGLY

            // Getting the spacial coordinate
//            const GlobalPosition & globalPosCurrent
//                = elementIt->geometry().corner(scvIdx);

            const GlobalPosition & globalPosCurrent
                = fvElemGeom.subContVol[scvIdx].global;

            Scalar saturationW  = elemVolVars[scvIdx].fluidState().saturation(wPhaseIdx);
            Scalar Tw           = elemVolVars[scvIdx].fluidState().temperature(wPhaseIdx); // 0; //
            Scalar Tn           = elemVolVars[scvIdx].fluidState().temperature(nPhaseIdx);
            Scalar Ts           = elemVolVars[scvIdx].fluidState().temperature(sPhaseIdx);
            Scalar ReW          = elemVolVars[scvIdx].reynoldsNumber(wPhaseIdx);
            Scalar ReN          = elemVolVars[scvIdx].reynoldsNumber(nPhaseIdx);

            time            = problem.timeManager().time();
            timeStepIndex   = problem.timeManager().timeStepIndex() ;

            /*
             * For the initial solution index and time are correct.
             * Afterwards this method is called in problem.postTimestep() , but the time(Idx) is not yet post.
             */
            if (not initialFlag){
                time += problem.timeManager().timeStepSize();
                timeStepIndex ++ ;
            }

            // filename of the output file
            std::string fileName = problem.name();
            fileName+=".dat";

            std::ofstream dataFile;

            // Writing a header into the output
            if (firstHeaderFlag == true)
            {
                dataFile.open(fileName.c_str());
                dataFile << "# This is a DuMuX output file for further processing with the preferred graphics program of your choice. \n";

                dataFile << "# This output file was written from "<< __FILE__ << ", line " <<__LINE__ << "\n";
                dataFile << "# This output file was generated from code compiled at " << __TIME__ <<", "<< __DATE__<< "\n";
                dataFile << "\n";
                dataFile << "# Header\n";
                dataFile << "#timestep\t time\t\t \t\t x \t\tSw \t\t\t Tw\t\t Tn\t Ts \t ReW \t ReN" << std::endl;
                dataFile.close();
                firstHeaderFlag = false ;
            }

            // actual output into the text file
            // This could be done much more efficiently
            // if writing to the text file would be done all at once.
            dataFile.open(fileName.c_str() , std::ios::app);
            dataFile << timeStepIndex
                    <<"\t\t\t"
                    << time
                    << "\t\t\t"
                    << globalPosCurrent[0];

            dataFile <<"\t\t\t"
                        << saturationW
                        <<"\t\t" << Tw
                        <<"\t\t" << Tn
                        <<"\t\t" << Ts
                        <<"\t\t" << ReW
                        <<"\t\t" << ReN

                        ;
            dataFile <<"\n";
            dataFile.close();
                // In order to get the entry of the right hand side:
                // This is the entry of the other scvIdx of the last element
                // pasing element is always one further than the iterator
                if(pasingElementIt == endIt){
                    fvElemGeom.update(problem.gridView(), *elementIt);
                    elemVolVars.update(problem, *elementIt, fvElemGeom, false);

                    int scvIdx = 1 ; // because I want to visit each location only once i.e. UGLY

                    const GlobalPosition & globalPosCurrent
                        = fvElemGeom.subContVol[scvIdx].global;

                    Scalar saturationW  = elemVolVars[scvIdx].fluidState().saturation(wPhaseIdx);
                    Scalar Tw           = elemVolVars[scvIdx].fluidState().temperature(wPhaseIdx); // 0; //
                    Scalar Tn           = elemVolVars[scvIdx].fluidState().temperature(nPhaseIdx);
                    Scalar Ts           = elemVolVars[scvIdx].temperature(sPhaseIdx);

                    Scalar ReW           = elemVolVars[scvIdx].reynoldsNumber(wPhaseIdx);
                    Scalar ReN           = elemVolVars[scvIdx].reynoldsNumber(nPhaseIdx);

                    // actual output into the text file
                      // This could be done much more efficiently
                      // if writing to the text file would be done all at once.
                      dataFile.open(fileName.c_str() , std::ios::app);
                      dataFile << timeStepIndex
                              <<"\t\t\t"
                              << time
                              << "\t\t\t"
                              << globalPosCurrent[0];

                      dataFile <<"\t\t\t"
                                  << saturationW
                                  <<"\t\t" << Tw
                                  <<"\t\t" << Tn
                                  <<"\t\t" << Ts
                                  <<"\t\t" << ReW
                                  <<"\t\t" << ReN
                                  ;
                      dataFile <<"\n";
                      dataFile.close();
                }
                ++pasingElementIt;
            }
        initialFlag =false;
    return;
    }
};













































///*!
// * \brief A collection of functions for output purposes.
// *          These output files are meant for visualization with another program (matlab, gnuplot...)
// */
//template<class TypeTag>
//class OutputToFile
//{
//    typedef typename GET_PROP_TYPE(TypeTag, Scalar)               Scalar;
//    typedef typename GET_PROP_TYPE(TypeTag, Problem)              Problem;
//    typedef typename GET_PROP_TYPE(TypeTag, GridView)             GridView;
//    typedef typename GridView::template Codim<0>::Entity                Element;
//    typedef typename GridView::template Codim<0>::Iterator              ElementIterator;
//
//    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry)    FVElementGeometry;
//    typedef typename GET_PROP_TYPE(TypeTag, DofMapper)            DofMapper;
//    typedef typename GET_PROP_TYPE(TypeTag, ElementSolutionVector) ElementSolutionVector;
//    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector)       SolutionVector;
//    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw)          MaterialLaw;
//    typedef typename MaterialLaw::Params                                MaterialLawParams;
//
//    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
////    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
//    typedef typename GET_PROP_TYPE(TypeTag, TwoPIAIndices) Indices;
//
//
//
//    enum {
//        numEq = GET_PROP_VALUE(TypeTag, NumEq),
//
//        wPhaseIdx = Indices::wPhaseIdx,
//        nPhaseIdx = Indices::nPhaseIdx,
//
//        // Grid and world dimension
//        dim         = GridView::dimension,
//        dimWorld    = GridView::dimensionworld,
//    };
//    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
//
//public:
//    /*
//     * \brief A function that puts Sw, pc, awn data (for one point in the simulation domain) into columns and saves it to a text file.
//     *
//     *          Each line contains the resulting values of one time step.
//     *          These output files are meant for visualization with another program (matlab, gnuplot...)
//     */
//    void smallResultWriter(const Problem & problem, const GlobalPosition & plotGlobalPosition)
//    {
//        //BEWARE globalIdx is not the same as paraview index !!! therefore selcection of the node has to be done via coordinates :-(
//        static bool firstFlag = true;
//
//        typedef typename GridView::template Codim<0>::Entity Element;
//        typedef typename GridView::template Codim<0>::Iterator ElementIterator;
//        typedef typename GET_PROP_TYPE(TypeTag, DofMapper) DofMapper;
//        typedef typename GET_PROP_TYPE(TypeTag, ElementSolutionVector) ElementSolutionVector;
//        typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;
//
//        ElementSolutionVector tmpSol;
//
////        SolutionVector & globalSol = problem.model().curSol();
////    //                SolutionVector & oldGlobalSol = problem.model().prevSol();
//
//        Scalar satWIdx[10] ={0.};
//        Scalar pcIdx[10]={0.};
//        Scalar awnIdx[10]={0.};
//        Scalar pcBounding[10]={0.};
//        Scalar dSw[10]={0.};
//        Scalar dt[10]={0.};
//        Scalar dSwdt[10]={0.};
//        Scalar x1 = plotGlobalPosition[0];
//        Scalar y1 = plotGlobalPosition[1];
//
//        int numKnots; // for output loop (more than one node can be put to the file)
//        int scvIdx;
//
//        FVElementGeometry fvElemGeom;
//        ElementVolumeVariables elemVolVars;
//        ElementVolumeVariables prevElemVolVars;
//
//        ElementIterator endit = problem.gridView().template end<0>();
//        for (ElementIterator elementIt = problem.gridView().template begin<0>() ; elementIt != endit; ++elementIt)
//        {
//
//            fvElemGeom.update(problem.gridView(), *elementIt);
//            elemVolVars.update(problem, *elementIt, fvElemGeom, false);
//            prevElemVolVars.update(problem, *elementIt, fvElemGeom, true);
//
//            int numVerts = elementIt->template count<dim>();
//            for (int i = 0; i < numVerts; ++i)
//            {
//                scvIdx = i;
//
//                const MaterialLawParams &pcParamsDrainageInit =
//                                problem.spatialParameters().materialLawParamsDrainage(*elementIt, fvElemGeom, scvIdx);
//
//                numKnots = 0;
//                const GlobalPosition & curGlobalPos = fvElemGeom.subContVol[i].global;
//
//                //BEWARE globalIdx is not the same as paraview index !!!
//                if(curGlobalPos[0]<x1+eps_ and curGlobalPos[0]>x1-eps_ and curGlobalPos[1]< y1 +eps_ and curGlobalPos[1]>y1-eps_) // 2D
////                if(curGlobalPos[0]<x1+eps_ and curGlobalPos[0]>x1-eps_ and curGlobalPos[1]<y1+eps_ and curGlobalPos[1]>y1-eps_ and curGlobalPos[2]<z1+eps_ and curGlobalPos[2]>z1-eps_)// 3D
//                {
//
//                    satWIdx[numKnots]   = elemVolVars[i].saturation(wPhaseIdx);
//                    pcIdx[numKnots]     = elemVolVars[i].capillaryPressure();
////                    pcBounding[numKnots]= MaterialLaw::pC(pcParamsDrainageInit, satWIdx[numKnots]); // this is the pc on the bounding curve
//                    awnIdx[numKnots]    = elemVolVars[i].awn() + 50 ;
//
//                    Scalar Sw			= elemVolVars[i].saturation(wPhaseIdx);
//                    Scalar SwOld		= prevElemVolVars[i].saturation(wPhaseIdx);
//                    dSw[numKnots]         = (Sw-SwOld);
//                    dt[numKnots]         = problem.timeManager().timeStepSize();
//                    dSwdt[numKnots]        = dSw[numKnots] / dt[numKnots];
//                }
//                numKnots++;
//
////                if(curGlobalPos[0]<x2+eps_ and curGlobalPos[0]>x2-eps_ and curGlobalPos[1]<y2+eps_ and curGlobalPos[1]>y2-eps_) // 2D
////				{
////                    satWIdx[numKnots]   = elemVolVars[i].saturation(wPhaseIdx);
////                    pcIdx[numKnots]     = elemVolVars[i].capillaryPressure();
//////                    pcBounding[numKnots]= MaterialLaw::pC(pcParamsDrainageInit, satWIdx[numKnots]); // this is the pc on the bounding curve
////                    awnIdx[numKnots]    = elemVolVars[i].awn();
////
////                    Scalar Sw			= elemVolVars[i].saturation(wPhaseIdx);
////                    Scalar SwOld		= prevElemVolVars[i].saturation(wPhaseIdx);
////                    dSw[numKnots]         = (Sw-SwOld);
//////                                std::cout<<"Sw: " << Sw << "SwOld: " << SwOld << "dSw " << dSw[numKnots] <<"\n";
////                    dt[numKnots]         = problem.timeManager().timeStepSize();
////                    dSwdt[numKnots]        = dSw[numKnots] / dt[numKnots];
////				}
////				numKnots++;
//    //                        if (globalIdx == 51) use COORDINATES !!!
//    //                        {
//    //
//    ////                            problem.model().localJacobian().restrictToElement(tmpSol, oldGlobalSol);
//    ////                            problem.model().localJacobian().setPreviousSolution(tmpSol);
//    ////
//    ////                            Scalar SwOld = problem.model().localJacobian().prevElemDat()[i].saturation(wPhaseIdx);
//    ////
//    ////                            problem.model().localJacobian().restrictToElement(tmpSol, globalSol);
//    ////                            problem.model().localJacobian().setCurrentSolution(tmpSol);
//    //
//    //                            satWIdx[numKnots]      = problem.model().localJacobian().curElemDat()[i].saturation(wPhaseIdx);
//    //                            pcIdx[numKnots]     = problem.model().localJacobian().curElemDat()[i].capillaryPressure();
//    //                            pcBounding[numKnots]= MaterialLaw::pC(pcParamsDrainageInit, satWIdx[numKnots]); // this is where it should be
//    //                            awnIdx[numKnots]    = problem.model().localJacobian().curElemDat()[i].awn();
//    //
//    //                            dSw[numKnots]         = (problem.model().localJacobian().curElemDat()[i].saturation(wPhaseIdx)
//    //                                                        - SwOld );
//    ////                            Scalar Sw = problem.model().localJacobian().curElemDat()[i].saturation(wPhaseIdx);
//    ////                            cout<<"Sw: " << Sw << "SwOld: " << SwOld << "dSw " << dSw[numKnots] <<"\n";
//    //                            dt[numKnots]         = problem.model().problem().timeManager().timeStepSize();
//    //                            dSwdt[numKnots]        = dSw[numKnots] / dt[numKnots];
//    //                        }
//    //                        numKnots++;
//            }
//        }
//
//    //                Scalar time = problem.timeManager().time();
//    //                Scalar timeStep = problem.timeManager().timeStepSize();
//        int timeStepIndex = problem.timeManager().timeStepIndex();
//
//        std::string dataFileName = problem.name();
//        std::string fileName = (boost::format("%s.dat")%dataFileName).str();
//
//        std::ofstream dataFile;
//
//        if (firstFlag == true)
//        {
//            dataFile.open(fileName.c_str());
//            dataFile << "# This is an DuMuX output file for further processing with the preferred graphics program of your choice. \n";
//            dataFile << "# These Numbers are printed in order to check whether the data points are one the bounding curve, as they should be.\n";
//
//            dataFile << "# This output file was written from "<< __FILE__ << ", line " <<__LINE__ << "\n";
//            dataFile << "# This output file was generated from code compiled at: " << __TIME__ <<", "<< __DATE__<< "\n";
//            dataFile << "\n";
//            dataFile << "# Header\n";
//    //                    dataFile << "# \ttime \ttimestep \ttotal_CO2_mass \ttotal_brine_mass \tCO2_flux_across_z_=_80_m \tCPUtime" <<std::endl;
//            dataFile << "#timestep \tSwIdx0 \t\tpcIdx0 \t\tpcBoundingIdx0 " << std::endl;
//            dataFile.close();
//
//            firstFlag = false ;
//        }
//        dataFile.open(fileName.c_str() , std::ios::app);
//    //                dataFile <<timeStepIndex<<"\t" << time <<"\t"<< timeStep <<"\t\t"<< co2Mass <<"\t\t"<< brineMass <<"\t\t"<< co2Flux <<"\t\t\t"<<"\t"<<CPUtime<< "\n";
//        dataFile << timeStepIndex ;
//        for (int i=0; i<numKnots; i++)
//        {
//            dataFile <<"\t"<<satWIdx[i] <<"\t"<< pcIdx[i]  <<"\t"<< pcBounding[i] <<"\t"<< awnIdx[i] ;
//        }
//        dataFile <<"\n";
//        dataFile.close();
//    return;
//    }


    /*
     * \brief A function that puts Sw, pc or Sw, pc, awn into a file.
     *          Right now only the derivatis of the awn surface are evaluated.
     *          This is for checking whether the parameters (e.g. a1...6 of the awn surface) are put in correctly, The regularization is smooth etc...
     *          These output files are meant for visualization with another program (matlab, gnuplot...)
     */
    /*
    void smallMaterialLawWriter(Problem & problem)
    {
        typedef typename GET_PROP_TYPE(TypeTag, AwnSurface) AwnSurface;
        typedef typename GET_PROP_TYPE(TypeTag, AwnSurfaceParams) AwnSurfaceParams;

        int scvIdx;
        int numSteps = 21; // this must not be to big -> segfault (if it has to be big: may be make std::)vector
        Scalar Swmax = 1. , Swmin =0.;
        Scalar pcmax = 12000., pcmin =0.;
        Scalar deltaSw = (double) (Swmax - Swmin) / numSteps;
        Scalar deltapc = (double) (pcmax - pcmin) / numSteps;
        const int sizeArray = numSteps*numSteps ;
        double Sw[sizeArray] ;
        double pc[sizeArray] ;
        double dawndpc[sizeArray], dawndSw[sizeArray] ;
        double SwTemp(0.), pcTemp(0.), awnTemp(0.), dawndpcTemp(0.), dawndSwTemp(0.);

        AwnSurfaceParams awnSurfaceParams_;
        MaterialLawParams pcParamsDrainageInit;


        FVElementGeometry fvElemGeom;

        ElementIterator endit = problem.gridView().template end<0>();
        for (ElementIterator elementIt = problem.gridView().template begin<0>() ; elementIt != endit; ++elementIt)
        {

            fvElemGeom.update(problem.gridView(), *elementIt);

            int numVerts = elementIt->template count<dim>();


            for (int i = 0; i < numVerts; ++i)
            {
                scvIdx = i;
                awnSurfaceParams_ = this->spatialParameters().awnSurfaceParams(*elementIt, fvElemGeom, scvIdx);

                pcParamsDrainageInit =
                                problem.spatialParameters().materialLawParamsDrainage(*elementIt, fvElemGeom, scvIdx);
            }
        }



        static bool firstFlag = true;

        std::string dataFileName = this->name();
        std::string fileName = (boost::format("%s_dawndSw_dawndpc.dat")%dataFileName).str();
        std::ofstream dataFile;

        for(int horse=0; horse <numSteps; horse++ )
        {
            for (int deer=0; deer < numSteps; deer++)
            {
                if (firstFlag == true)
                {
                    dataFile.open(fileName.c_str());
                    dataFile << "# This is an DuMuX output file for further processing with the preferred graphics program of your choice. \n";
                    dataFile << "# These Numbers are for comparison whether the input material laws (coefficients) are 'correct', i.e. visualization with another program.\n";

                    dataFile << "# This output file was written from "<< __FILE__ << ", line " <<__LINE__ << "\n";
                    dataFile << "# This output file was generated from code compiled at " << __TIME__ <<", "<< __DATE__<< "\n";
                    dataFile << "\n";
                    dataFile << "# Header\n";
                    dataFile << "# \tSw \t\tpc \t\t dawndSw \t\t dawndpc " <<std::endl;
                    dataFile.close();
                    firstFlag = false ;
                }


                dawndSwTemp = AwnSurface::dawndSw(awnSurfaceParams_, SwTemp, pcTemp ) ;
                dawndpcTemp = AwnSurface::dawndpc(awnSurfaceParams_, SwTemp, pcTemp ) ;

                Sw[horse*numSteps + deer]         = SwTemp;
                pc[horse*numSteps + deer]         = pcTemp;

                dawndSw[horse*numSteps + deer]     = dawndSwTemp;
                dawndpc[horse*numSteps + deer]     = dawndpcTemp;

                SwTemp += deltaSw;

            }

            SwTemp=0.;
            pcTemp += deltapc;
        }
        dataFile.open(fileName.c_str() , std::ios::app);
        for (int mule =0 ; mule < sizeArray; mule++)
        {
            dataFile << Sw[mule] <<"\t" << pc[mule] << "\t" << dawndSw[mule] << "\t" << dawndpc[mule] << "\n";
        }
        dataFile.close();
        exit (1);
        return ;
    }
    */

/*
    // --------------------------------------------------------------------------------------------------------------------------------------------------
     // --------------------------------------------------------------------------------------------------------------------------------------------------
     // --------------------------------------------------------------------------------------------------------------------------------------------------
     // This block of code can bepasted into the respective psiFunction after the parameters have been assigned.
     // also the axxording line after psiTemp has to be uncommented.
     // AFter finally assigning an approprite fileName one can get an control output of these input functions

                 static bool firstFlag = true;
                 std::string dataFileName = "testing";
                 std::string fileName = (boost::format("%s_psi_wetting_interfacialArea.dat")%dataFileName).str();
                 std::ofstream dataFile;
                 const int numSteps = 1000;
                 const Scalar Swmax = 1. , Swmin =0.;
                 const Scalar deltaSw = (double) (Swmax - Swmin) / numSteps;
                 const int sizeArray = numSteps ;
                 Scalar psiTemp;
                 Scalar psi[sizeArray], SwOut[sizeArray];

     			for (int deer=0; deer < numSteps; deer++)
     			{
     				if (firstFlag == true)
     				{
     					dataFile.open(fileName.c_str());
     					dataFile << "# This is an DuMuX output file for further processing with the preferred graphics program of your choice. \n";
     					dataFile << "# These Numbers are for comparison whether the input material laws (coefficients) are 'correct', i.e. visualization with another program.\n";

     					dataFile << "# This output file was written from "<< __FILE__ << ", line " <<__LINE__ << "\n";
     					dataFile << "# This output file was generated from code compiled at " << __TIME__ <<", "<< __DATE__<< "\n";
     					dataFile << "\n";
     					dataFile << "# Header\n";
     					dataFile << "# \t Sw \t\t Psi " <<std::endl;
     					dataFile.close();
     					firstFlag = false ;
     					Sw = 0.;
     				}
//        			   psiTemp = - a * pow(Sw, b) *1000. ; // psi_nonWetting_wetting
//        				psiTemp = a / (1 + b * exp(-c * Sw))     ; // psi_nonWetting_saturation
//            			   psiTemp =  a + b * Sw / log(Sw) + c*log(Sw)/pow(Sw,2);         ;//psi_wetting_saturation
//            			   psiTemp = a / (1 + b * exp(-1.* c * Sw)); // psi_nonWetting_interfacialArea
         			   psiTemp =    a + b * Sw /log(Sw) + c * log (Sw) /pow(Sw,2)   ; // psi_wetting_interfacialArea

     			   psi[deer]	= psiTemp ;
     			   SwOut[deer] 	= Sw;

     			   Sw	 		+= deltaSw ;
     			}

                 dataFile.open(fileName.c_str() , std::ios::app);
                 for (int mule =0 ; mule < sizeArray; mule++)
                 {
                     dataFile << SwOut[mule] <<"\t" << psi[mule]  << "\n";
                 }
                 dataFile.close();
                 std::cout << "Terminated from file: " << __FILE__ << " line: " << __LINE__ << "\n";
                 exit (1);
     // --------------------------------------------------------------------------------------------------------------------------------------------------
     // --------------------------------------------------------------------------------------------------------------------------------------------------
     // --------------------------------------------------------------------------------------------------------------------------------------------------
             */

// // // can be used for printing e.g. material laws to the screen
//    std::cout << "Sw \t pc \n" ;
//    Scalar SwOut =pcParamsDrainage.Swr();
//    for (int i=1; i<1000; i++)
//    {
//    	SwOut += (1./1000.* (1-pcParamsDrainage.Swr() ));
//    	Scalar xOfSw;
//    	xOfSw = MaterialLaw::pC(pcParamsDrainage, SwOut);
//    	std::cout << SwOut << " " << xOfSw << " " << SwOut << " \n" ;
//    }
//    exit(1);
//};
}
#endif
