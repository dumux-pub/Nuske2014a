Summary
=======
This is the DuMuX module containing the code for producing the results
published in:

P. Nuske, V. Joekar-Niasar and R. Helmig,<br>
[Non-equilibrium in multiphase multicomponent flow in porous media: An evaporation example]
(http://www.sciencedirect.com/science/article/pii/S0017931014002178)<br>
International Journal of Heat and Mass Transfer, 2014<br>
DOI:  10.1016/j.ijheatmasstransfer.2014.03.011

You can use the .bib file provided [here](Nuske2014a.bib).

Installation
============

The easiest way is to use the `installNuske2014a.sh` script in this folder.
You might want to look at it before execution [here](https://git.iws.uni-stuttgart.de/dumux-pub/Nuske2014a/raw/master/installNuske2014a.sh). Create a new folder containing the script and execute it.

Alternatively,
you can copy the following to a terminal:
```bash
mkdir -p Nuske2014a && cd Nuske2014a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Nuske2014a/raw/master/installNuske2014a.sh
chmod +x installNuske2014a.sh && ./installNuske2014a.sh
```
It will install this module will take care of most dependencies.
For the basic dependencies see dune-project.org and at the end of this README.


For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.


Applications
============

The application used for this publications can be found in `appl/kinetic/micromodel`.


Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installation.sh](hhttps://git.iws.uni-stuttgart.de/dumux-pub/Nuske2014a/raw/master/installNuske2014a.sh).


The module has been checked for the following compilers / tools:

| compiler /tool| version |
| ------------- | ------- |
| clang/clang++ | 3.5     |
| gcc/g++       | 5.3     |
| automake      | 1.13.4  |