%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %                                                                           
 %   This program is free software: you can redistribute it and/or modify    
 %   it under the terms of the GNU General Public License as published by 
 %   the Free Software Foundation, either version 2 of the License, or       
 %   (at your option) any later version.                                     
 %                                                                           
 %   This program is distributed in the hope that it will be useful,         
 %   but WITHOUT ANY WARRANTY; without even the implied warranty of          
 %   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         
 %   GNU General Public License for more details.                            
 %                                                                           
 %   You should have received a copy of the GNU General Public License       
 %   along with this program.  If not, see <http://www.gnu.org/licenses/>.   
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function len = estimateCurveLength(im)

%
%  create lookup table for pixels contribution to curve length
%  depending on its neighbour
%

raylength = 0.5*[sqrt(2) 1 sqrt(2) 1 sqrt(2) 1 sqrt(2) 1];

for n=1:255
  bits = bitget(n,8:-1:1);
  curvelen(n) = sum(bits.*raylength);
%  disp([dec2bin(n,8) ' = ' num2str(curvelen(n))]);
end


%
%  special case endpart
%
curvelen(bin2dec('10000000')) = sqrt(2);
curvelen(bin2dec('01000000')) = 1;
curvelen(bin2dec('00100000')) = sqrt(2);
curvelen(bin2dec('00010000')) = 1;
curvelen(bin2dec('00001000')) = sqrt(2);
curvelen(bin2dec('00000100')) = 1;
curvelen(bin2dec('00000010')) = sqrt(2);
curvelen(bin2dec('00000001')) = 1;

%
%  special case sqrt(1 + 2^2) = sqrt(5)
%
curvelen(bin2dec('10010000')) = sqrt(5)/2;
curvelen(bin2dec('01001000')) = sqrt(5)/2;
curvelen(bin2dec('00100100')) = sqrt(5)/2;
curvelen(bin2dec('00010010')) = sqrt(5)/2;
curvelen(bin2dec('00001001')) = sqrt(5)/2;
curvelen(bin2dec('10000100')) = sqrt(5)/2;
curvelen(bin2dec('01000010')) = sqrt(5)/2;
curvelen(bin2dec('00100001')) = sqrt(5)/2;

%
%  compute "length" for each pixel
%
mask = double(im > 0);
idx = find( mask == 1);

lenImage = zeros( size(im));
len = 0;
for n = 1:length(idx)
  [x,y] = ind2sub(size(im), idx(n));
  if( x >= 2 && x <= size(im,1)-1 && y >= 2 && y <= size(im,2)-1)
	neighborcode = ...
		128  * mask(x-1,y-1) ...
		+ 64 * mask(x-1,y  ) ...
		+ 32 * mask(x-1,y+1) ...
		+ 16 * mask(x  ,y+1) ...
		+ 8  * mask(x+1,y+1) ...
		+ 4  * mask(x+1,y  ) ...
		+ 2  * mask(x+1,y-1) ...
		+ 1  * mask(x  ,y-1);
	if( neighborcode > 0)
	  pixlen = curvelen(neighborcode);
	  lenImage(idx(n)) = pixlen;
	  len = len + pixlen;
	end
  end
end

%imshow( lenImage,[]); colormap jet

