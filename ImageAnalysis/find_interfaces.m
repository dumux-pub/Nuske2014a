%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %                                                                           
 %   This program is free software: you can redistribute it and/or modify    
 %   it under the terms of the GNU General Public License as published by 
 %   the Free Software Foundation, either version 2 of the License, or       
 %   (at your option) any later version.                                     
 %                                                                           
 %   This program is distributed in the hope that it will be useful,         
 %   but WITHOUT ANY WARRANTY; without even the implied warranty of          
 %   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         
 %   GNU General Public License for more details.                            
 %                                                                           
 %   You should have received a copy of the GNU General Public License       
 %   along with this program.  If not, see <http://www.gnu.org/licenses/>.   
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculate Resolution of the theoretical Mask -- Camera at 11µm / pixel
%

path='./' ; 

load('correspondences.mat'); 

mFixed = mean( fixedPoints);
fixedPointsMeanFree = fixedPoints - repmat(mFixed,[size( fixedPoints,1),1]);
stdFixed = sqrt(mean((fixedPointsMeanFree(:)).^2));

mMoving = mean( movingPoints);
movingPointsMeanFree = movingPoints - repmat(mMoving,[size( movingPoints,1),1]);
stdMoving = sqrt(mean((movingPointsMeanFree(:)).^2));

scaleFactor = stdFixed/stdMoving ;

moving_elemsize_um = 11;
fixed_elemsize_um = moving_elemsize_um / scaleFactor;
fixed_elemsize_mm = fixed_elemsize_um / 1000;

%
%  eliminate dirt / small stuff
%
cd ([path,'QuasiStaticMeasurementData']) 

emptyfilename = 'camera4-30.tif';

reffilename = '../theoreticalNetwork.png';

filstubs = {...
	'camera4-31',...
	'camera4-32',...
	'camera4-33',...
	'camera4-34',...
	'camera4-35',...
	'camera4-36',...
	'camera4-37',...
	'camera4-38',...
	'camera4-39',...
	'camera4-40'};

filstubs_out = filstubs;

fi=1 ;
infilename = [filstubs{fi} '.tif'];
a = (imread( infilename));
ref = imread(reffilename);
emptyim = im2double(imread(emptyfilename));

%
%  crop network to resonable size
%
xmin = 740;
xmax = 2300;
ymin = 330;
ymax = 1260;
ref = ref( ymin:ymax, xmin:xmax);

%
%  create network mask and borders
%
refmask = ref(:,:,1)>0;
borders = refmask - imerode( refmask, strel('disk',1));
borders = borders>0;

%
%  load correspondence points (manually created with cpselect(a,ref))
%  and adjust them to cropped network
%
load('correspondences.mat'); 
fixedPoints = fixedPoints - repmat([xmin-1 ymin-1], ...
									   [size(fixedPoints,1) 1]);
%
%  Compute thin plate spline elastic transformation for camera
%  images to network imageg
%
st = tpaps(fixedPoints', movingPoints', 1);
[X, Y] = meshgrid( 1:size(ref,2), 1:size(ref,1));
transpoints = fnval(st, [X(:), Y(:)]');
Xsrc = reshape( transpoints(1,:), size(ref));
Ysrc = reshape( transpoints(2,:), size(ref));

%
%  smoothing kernel for shading correction
%
sigma = 100;
x = -200:200;
kern = exp(-x.^2 / sigma^2);
kern = kern/sum(kern(:));


%
%  run over all given images
%
result = [];
mkdir ../resultImages
for fi=1:length(filstubs)
% for fi=1:5:6
  %  read input camera image
  %
  infilename = [filstubs{fi} '.tif'];
  a = double(imread( infilename));
  
  %
  %  elastic transformation to network image
  %
  raw_transformed = interp2( a/255, Xsrc, Ysrc, 'cubic', 0);
  
  %
  %  create a mask with valid camera pixels in network image
  %
  validmask = interp2( ones(size(a)), Xsrc, Ysrc, 'cubic', 0);
  validmask = (validmask>0.5);
  validmask = imerode(validmask, strel('disk',1));
  
  %
  %  in first iteration, compute shading correction (using
  %  intensities between network channels)
  %
  if( fi == 1)
	bg = raw_transformed .* imerode( ~refmask, strel('disk',5));
	bg1 = imfilter(imfilter( double(bg), kern), kern');
	bg2 = imfilter(imfilter( double(bg>0),  kern), kern');
	bgsmooth = bg1./bg2;
  end
  
  %
  %  shading correction of transformed camera image
  %
  raw_bg_corr = raw_transformed ./ bgsmooth;
  
  %
  %  create mask of black phase by threshold on slightly smoothed
  %  camera image
  %
  raw_bg_corr2 = imfilter( raw_bg_corr, fspecial( 'gauss', 7, 1));
  bp1 = raw_bg_corr2 < 0.5;
  bp1 = bp1 .* validmask;
  
  %
  %  valid regions inside the network channels are 2 pixels away
  %  from the border. 
  %  limit black phase to valid regions
  %
  refmask_erod = imerode(refmask,strel('disk',2));
  bp2 = bp1 .* refmask_erod;
  
  %
  %  Delete small dust segments ( < 10 pixels)
  %
  CC=bwconncomp(bp2);
  numPixels = cellfun(@numel,CC.PixelIdxList);
  idx = find(numPixels < 10);
  for n=1:length(idx)
	bp2(CC.PixelIdxList{idx(n)}) = 0;
  end

  %
  %  mask of bright phase is just the inverse of the black phase
  %
  lp2 = (~bp1) .* refmask_erod .* validmask;
  
  %
  %  find borders between dark phase, bright phase and walls (interfaces)
  %
  active_borders_bp = imdilate( bp2,  strel('disk',1)) & ~refmask_erod;
  active_borders_lp = imdilate( lp2,  strel('disk',1)) & ~refmask_erod;
  contact = imdilate( bp2,  strel('disk',1)) & lp2;
  
  
  %
  %  in the first image (empty image) use all three masks to define
  %  dust-particle borders
  %
  if( fi ==1)
	dustparticleborders = imdilate(active_borders_bp ...
								   | contact, strel('disk',1));
	dustparticlemask = imdilate( bp2,  strel('disk',1));
  end
  
  %
  %  remove wrong broders from dust particles
  %
  active_borders_bp = active_borders_bp & ~dustparticleborders;
  active_borders_lp = active_borders_lp & ~dustparticleborders;
  contact = contact & ~dustparticleborders;
  bp2 = bp2 &~dustparticlemask;
  lp2 = lp2 &~dustparticlemask;
  
  %
  %  measure relevant quanitites 
  %
  result(fi).filename = infilename;
  result(fi).dark_mask = bp2;
  result(fi).bright_mask = lp2;
  result(fi).area_dark   = sum(bp2(:)) * fixed_elemsize_mm^2;
  result(fi).area_bright = sum(lp2(:)) * fixed_elemsize_mm^2;
  result(fi).interface_dark_wall = estimateCurveLength(active_borders_bp) ...
	  * fixed_elemsize_mm;
  result(fi).interface_bright_wall = estimateCurveLength(active_borders_lp) ...
	  * fixed_elemsize_mm;
  result(fi).interface_dark_bright = estimateCurveLength(contact) * ...
	  fixed_elemsize_mm;

  %
  %  create nice output image
  %
  raw_transformed_wo_borders = raw_transformed;
  raw_transformed_wo_borders(active_borders_bp | active_borders_lp | ...
							 contact ) = 0;
  rgb = cat(3,...
			raw_transformed_wo_borders + active_borders_bp+contact,...
			raw_transformed_wo_borders + active_borders_lp+contact,...
			raw_transformed_wo_borders + borders );

       
  pos = [0 0; 0 1; 0 2; 0 3; 0 4]*21;
  txt = {['dark area:              ' num2str(result(fi).area_dark) ' mm^2'],...
		 ['bright area:            ' num2str(result(fi).area_bright) ' mm^2'],...
		 ['interface dark--wall:   ' num2str(result(fi).interface_dark_wall) ' mm'],...
		 ['interface bright--wall: ' num2str(result(fi).interface_bright_wall) ' mm'],...
		 ['interface dark--bright: ' num2str(result(fi).interface_dark_bright) ' mm']} ; 
		 
%   rgb=insertText(rgb, pos, txt); & put in text if computer vision system
%   toolbox present
  figure(5)
  imshow( rgb)
  title( filstubs{fi})
  drawnow
  
  %
  %  save output image
  %
  imwrite( rgb,['../resultImages/', filstubs_out{fi} '.tif']);
end

cd ('..')   ; 

%
%  save results
%
save('result.mat', 'result');



