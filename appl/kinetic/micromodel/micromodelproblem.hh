// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*
 * \file MicroModelProblem.hh
 * \ingroup MpNcBoxproblems
 *
 * \brief Problem where hot, pure water is injected from the left hand side into a initially
 *        isotherm fluorinert (wetting) saturated domain. A kinetic model is used: i.e. three different (fluid, gas, solid)
 *        temperatures are primary variables.
 *
 *        The Problem is written, such that the kinetic consideration for mass and energy can
 *        be switched off by merely setting kinetic, kineticenergy respectivly to false.
 *        Boundary and initial conditions are specified for all cases.
 *
 * \author Philipp Nuske
 */
#ifndef DUMUX_MICROMODEL_PROBLEM_HH
#define DUMUX_MICROMODEL_PROBLEM_HH

#define MASS_TRANSFER_OFF 1
#warning MASS TRANSFER OFF (has to be off for micromodel fluidsystem: pure phases)

// this sets that the relation using pc_max is used.
// i.e. - only parameters for awn, ans are given,
//      - the fit for ans involves the maximum value for pc, where Sw, awn are zero.
// setting it here, because it impacts volume variables and spatialparameters
#define USE_PCMAX 0


#include <dumux/implicit/mpnc/mpncmodelkinetic.hh>

#include "micromodelspatialparams.hh"

#include <dumux/implicit/common/implicitporousmediaproblem.hh>

#include <dumux/material/fluidsystems/h2ofluorinert43fluidsystem.hh>
#include <dumux/implicit/mpnc/velomodelnewtoncontroller.hh>

#include <dumux/material/fluidstates/nonequilibriumfluidstate.hh>
#include <dumux/material/fluidstates/nonequilibriumenergyfluidstate.hh>
#include <dumux/material/fluidstates/nonequilibriummassfluidstate.hh>
#include <dumux/material/fluidstates/compositionalfluidstate.hh>

#include <dumux/io/plotoverline1d.hh>



#include <dumux/io/outputToFile.hh>

namespace Dumux
{

template <class TypeTag>
class MicroModelProblem;

namespace Properties
{

NEW_TYPE_TAG(MicroModelProblem,
             INHERITS_FROM(BoxMPNCKinetic, MicroModelSpatialParams));

// typedef Dune::SGrid<1, 1> type;
// Dune::OneDGrid
// Set the grid type
SET_TYPE_PROP(MicroModelProblem, Grid, Dune::OneDGrid);

// Set the problem property
SET_TYPE_PROP(MicroModelProblem,
              Problem,
              Dumux::MicroModelProblem<TTAG(MicroModelProblem)>);

// Set fluid configuration
SET_PROP(MicroModelProblem, FluidSystem)
{
private: typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:  typedef Dumux::FluidSystems::H2OFluorinert43</*floatType=*/Scalar, /*whether diffusion is considered=*/GET_PROP_VALUE(TypeTag, EnableDiffusion)> type;
};


// Set the newton controller
SET_TYPE_PROP(MicroModelProblem,
              NewtonController,
              Dumux::VeloModelNewtonController<TypeTag>);

//! Set the default pressure formulation: either pw first or pn first
SET_INT_PROP(MicroModelProblem,
             PressureFormulation,
/*hacked in volume variables-> hydrophic*/             MpNcPressureFormulation::leastWettingFirst/*do not change*/);


// Enable gravity
SET_BOOL_PROP(MicroModelProblem, ProblemEnableGravity, false);

// Specify whether diffusion is enabled
SET_BOOL_PROP(MicroModelProblem, EnableDiffusion, false);

// do not use smooth upwinding
SET_BOOL_PROP(MicroModelProblem, ImplicitEnableSmoothUpwinding, false);

// do not use a chopped newton method in the beginning
SET_BOOL_PROP(MicroModelProblem, NewtonEnableChop, false);

// use forward differences instead of central ones
SET_INT_PROP(MicroModelProblem, ImplicitNumericDifferenceMethod, 0);

// disable partial jacobian matrix reassembly
SET_BOOL_PROP(MicroModelProblem, ImplicitEnablePartialReassemble, false);

// Enable the re-use of the jacobian matrix whenever possible?
SET_BOOL_PROP(MicroModelProblem, ImplicitEnableJacobianRecycling, true);

// Specify whether the convergence rate ought to be written out by the
// newton method
SET_BOOL_PROP(MicroModelProblem, NewtonWriteConvergence, false);

// decide how to calculate velocity: Darcy / Forchheimer
//SET_TYPE_PROP(MicroModelProblem, BaseFluxVariables, ImplicitForchheimerFluxVariables<TypeTag>);

// SET_TYPE_PROP(MicroModelProblem, LinearSolver, SuperLUBackend<TypeTag>);

//#################
// Which Code to compile
// Specify whether there is any energy equation
SET_BOOL_PROP(MicroModelProblem, EnableEnergy, true);
// Specify whether the kinetic energy module is used
SET_INT_PROP(MicroModelProblem, NumEnergyEquations, 3);
// Specify whether the kinetic mass module is use
SET_BOOL_PROP(MicroModelProblem, EnableKinetic, true);
//#################

/*!
 * \brief The fluid state which is used by the volume variables to
 *        store the thermodynamic state. This has to be chosen
 *        appropriately for the model ((non-)isothermal, equilibrium, ...).
 */
SET_PROP(MicroModelProblem, FluidState){
    private:    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    private:    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
//    public: typedef Dumux::NonEquilibriumEnergyFluidState<TypeTag> type;
//    public: typedef Dumux::NonEquilibriumMassFluidState<TypeTag> type;
    public: typedef Dumux::NonEquilibriumFluidState<Scalar, FluidSystem> type;
//    public: typedef Dumux::CompositionalFluidState<Scalar, FluidSystem> type;
};

// Output variables
SET_BOOL_PROP(MicroModelProblem, VtkAddVelocities, false);
SET_BOOL_PROP(MicroModelProblem, VtkAddReynolds, true);
SET_BOOL_PROP(MicroModelProblem, VtkAddPrandtl, true);
SET_BOOL_PROP(MicroModelProblem, VtkAddNusselt, true);
SET_BOOL_PROP(MicroModelProblem, VtkAddInterfacialArea, true);
SET_BOOL_PROP(MicroModelProblem, VtkAddTemperatures, true);
SET_BOOL_PROP(MicroModelProblem, VtkAddInternalEnergies, true);
SET_BOOL_PROP(MicroModelProblem, VtkAddBoundaryTypes, true);


SET_BOOL_PROP(MicroModelProblem, VelocityAveragingInModel, true);

SET_BOOL_PROP(MicroModelProblem, VtkAddxEquil, true);
}

/*!
 * \ingroup MpNcBoxproblems
 *
 * \brief Problem where hot, pure air is injected from the left hand side into a initially
 *        isotherm domain. A kinetic model is used: i.e. three different (fluid, gas, solid)
 *        temperatures are primary variables.
 */
template <class TypeTag>
class MicroModelProblem
    : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef MicroModelProblem<TypeTag> ThisType;
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    /*!
     * \brief The fluid state which is used by the volume variables to
     *        store the thermodynamic state. This should be chosen
     *        appropriately for the model ((non-)isothermal, equilibrium, ...).
     *        This can be done in the problem.
     */
    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;
    typedef typename FluidSystem::ParameterCache ParameterCache;

    enum { S0Idx = Indices::s0Idx};
    enum { p0Idx = Indices::p0Idx};
    enum { conti00EqIdx    = Indices::conti0EqIdx };

    enum { dim = GridView::dimension};
    enum { dimWorld = GridView::dimensionworld }; // Grid and world dimension
    enum { numPhases       = GET_PROP_VALUE(TypeTag, NumPhases)};
    enum { numComponents   = GET_PROP_VALUE(TypeTag, NumComponents)};
    enum { temperature0Idx = Indices::temperatureIdx};
    enum { energyEq0Idx    = Indices::energyEqIdx};
    enum { numEnergyEqs    = Indices::numPrimaryEnergyVars};
    enum { wPhaseIdx       = FluidSystem::wPhaseIdx};
    enum { nPhaseIdx       = FluidSystem::nPhaseIdx};
    enum { sPhaseIdx       = FluidSystem::sPhaseIdx};
    enum { wCompIdx        = FluidSystem::wCompIdx};
    enum { nCompIdx        = FluidSystem::nCompIdx};
    enum { enableKinetic   = GET_PROP_VALUE(TypeTag, EnableKinetic)};
    enum { enableEnergy    = GET_PROP_VALUE(TypeTag, EnableEnergy)};
    enum { numEnergyEquations  		= GET_PROP_VALUE(TypeTag, NumEnergyEquations)};
    enum { velocityOutput  = GET_PROP_VALUE(TypeTag, VtkAddVelocities)};
    enum { velocityAveragingInModel	= GET_PROP_VALUE(TypeTag, VelocityAveragingInModel)};

    // formulations
    enum {
        pressureFormulation = GET_PROP_VALUE(TypeTag, PressureFormulation),
        mostWettingFirst    = MpNcPressureFormulation::mostWettingFirst,
        leastWettingFirst   = MpNcPressureFormulation::leastWettingFirst
    };


    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef Dune::FieldVector<typename GridView::Grid::ctype, dim> LocalPosition;
    typedef Dune::FieldVector<typename GridView::Grid::ctype, dimWorld> GlobalPosition;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLawParams) MaterialLawParams;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GET_PROP_TYPE(TypeTag, ElementBoundaryTypes) ElementBoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;

    typedef std::vector<Dune::FieldVector<Scalar, 1> >  ScalarVector;
    typedef std::array<ScalarVector, numPhases>         PhaseVector;
    typedef typename Dune::FieldVector<Scalar, numComponents> ComponentVector;
    typedef Dune::FieldVector<Scalar, dim>              DimVector;
    typedef Dune::BlockVector<DimVector>                DimVectorField;
    typedef std::array<DimVectorField, numPhases>       PhaseDimVectorField;

public:
    MicroModelProblem(TimeManager &timeManager,
    		const GridView &gridView)
        : ParentType(timeManager, gridView)
    {
        this->timeManager().startNextEpisode(1.);
    }

    void episodeEnd()
    {
        this->timeManager().startNextEpisode(1.);
    }

    void init()
    {
        try
        {
            eps_ 			        = 1e-8;
            outputName_             = GET_RUNTIME_PARAM(TypeTag, std::string, Constants.outputName);
            nRestart_               = GET_RUNTIME_PARAM(TypeTag, int, Constants.nRestart);
            massFluxInjectedPhase_  = GET_RUNTIME_PARAM(TypeTag, Scalar, BoundaryConditions.mInject);

            TInitial_               = GET_RUNTIME_PARAM(TypeTag, Scalar, InitialConditions.TInitial);
            SWaterInitial_              = GET_RUNTIME_PARAM(TypeTag, Scalar, InitialConditions.SWaterInitial);
            SWaterBoundary_             = GET_RUNTIME_PARAM(TypeTag, Scalar, BoundaryConditions.SWaterBoundary);
            pnInitial_              = GET_RUNTIME_PARAM(TypeTag, Scalar, InitialConditions.pnInitial);
            xInject_                = GET_RUNTIME_PARAM(TypeTag, Scalar, SourceSink.xInject);
            TInject_                = GET_RUNTIME_PARAM(TypeTag, Scalar, BoundaryConditions.TInject);
            injectionPhaseIdx_      = GET_RUNTIME_PARAM(TypeTag, Scalar, SourceSink.injectionPhaseIdx);
            TAmb_			      	= GET_RUNTIME_PARAM(TypeTag, Scalar, BoundaryConditions.TAmb);
            heatTransferCoeff_      = GET_RUNTIME_PARAM(TypeTag, Scalar, BoundaryConditions.heatTransferCoeff);
        }
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown!\n";
            exit(1);
        }

        this->spatialParams().setInputInitialize();


        Scalar S[numPhases];
        S[wPhaseIdx]    = SWaterInitial_;
        S[nPhaseIdx]    = 1- S[wPhaseIdx] ;

        Scalar p[numPhases];
        p[nPhaseIdx]    = pnInitial_ ;

        // capillary pressure parameters
        FluidState fluidState ;
        const MaterialLawParams &materialParams =
            this->spatialParams().materialLawParams();
        Scalar capPress[numPhases];

        //set saturation to inital values, this needs to be done in order for the fluidState to tell me pc
        for (int phaseIdx = 0; phaseIdx < numPhases ; ++phaseIdx) {
            fluidState.setSaturation(phaseIdx, S[phaseIdx]);
            if(enableEnergy){
                if (numEnergyEquations)
                    fluidState.setTemperature(phaseIdx,TInitial_);
                else
                    fluidState.setTemperature(TInitial_);
            }
        }

        //obtain pc according to saturation
        MaterialLaw::capillaryPressures(capPress, materialParams, fluidState);
        // set pressures from capillary pressures
        p[wPhaseIdx] = p[nPhaseIdx] + std::abs(capPress[wPhaseIdx]);


        const int otherIdx = (int) std::abs((injectionPhaseIdx_ - 1)) ; // if nInjected other:0, if wInjected other:1
        x_[injectionPhaseIdx_][injectionPhaseIdx_]  = xInject_ ;
        x_[injectionPhaseIdx_][otherIdx]            = 1.0 - x_[injectionPhaseIdx_][injectionPhaseIdx_] ;

        ParentType::init();
        this->model().initVelocityStuff();
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string name() const
    { return outputName_ ; }

    /*!
     * \brief Returns the temperature within the domain.
     */
    Scalar boxTemperature(const Element &element,
                          const FVElementGeometry &fvGeometry,
                          const unsigned int scvIdx) const
    { return TInitial_; };

    // \}


    /*!
     * \brief Called directly after the time integration.
     */
    void postTimeStep()
    {
        if (this->timeManager().episodeWillBeOver() ){
//			 write plot over Line to text files
//			 each output gets it's writer object in order to allow for different header files
			PlotOverLine1D<TypeTag> writer;

			// writer points: in the porous medium, not the outflow
			GlobalPosition pointOne ;
			GlobalPosition pointTwo ;

			pointOne[0] = this->bBoxMin()[0] ;
			pointTwo[0] = this->bBoxMax()[0] ;
			char buffer [50];
			const int epiIdx = this->timeManager().episodeIndex();
			sprintf(buffer, "plotOverLineEpisode%i",epiIdx) ;
			writer.write(*this,
						  pointOne,
						  pointTwo,
						  buffer);
        }

        // Calculate storage terms of the individual phases
        for (int phaseIdx = 0; phaseIdx < numEnergyEqs; ++phaseIdx) {
            PrimaryVariables phaseStorage(0);
            /* wieviel Erhaltungsgroesse im System ist (in Einheit der Bilanzgleichung)
             * Aufssummierte Storage term fuer das ganze System.
             */
            this->model().globalPhaseStorage(phaseStorage, phaseIdx);

            if (this->gridView().comm().rank() == 0) {
                std::cout
                    <<"Storage in  "
                    << FluidSystem::phaseName(phaseIdx)
                    << "Phase: ["
                    << phaseStorage
                    << "]"
                    << "\n";
            }
        }

        // Calculate total storage terms
        PrimaryVariables storage;
        this->model().globalStorage(storage);

        // Write mass balance information for rank 0
        if (this->gridView().comm().rank() == 0) {
            std::cout
                <<"Storage total: [" << storage << "]"
                << "\n";
        }
    }

    /*!
     * \brief Write a output file?
     */
    bool shouldWriteOutput() const
    {
    	const bool writeIt = (this->timeManager().episodeWillBeOver() or this->timeManager().timeStepIndex()==0 );
    	return  writeIt ;
    }



    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param values The boundary types for the conservation equations
     * \param vertex The vertex for which the boundary type is set
     */
    void boundaryTypes(BoundaryTypes &values, const Vertex &vertex) const
    {
        const GlobalPosition & globalPos = vertex.geometry().center();
        values.setAllDirichlet();
//        values.setAllNeumann();


//		for(int phaseIdx=0; phaseIdx < numPhases; ++ phaseIdx){
//			for(int compIdx=0; compIdx <numComponents; ++compIdx){
//				int offset = compIdx + phaseIdx * numComponents  ;
//				values.setNeumann(conti00EqIdx + offset);
//			}
//		}
//
//        if(enableEnergy or numEnergyEquations)
//            for (int energyEqIdx=0; energyEqIdx< numEnergyEqs; ++energyEqIdx)
//				values.setDirichlet(energyEq0Idx + energyEqIdx);

//

        if(onRightBoundary_(globalPos)){
            values.setAllNeumann();
//            values.setAllDirichlet();


//			for(int phaseIdx=0; phaseIdx < numPhases; ++ phaseIdx){
//				for(int compIdx=0; compIdx <numComponents; ++compIdx){
//					int offset = compIdx + phaseIdx * numComponents  ;
//					values.setDirichlet(conti00EqIdx + offset);
//				}
//			}
//
//	        if(enableEnergy or numEnergyEquations)
//	            for (int energyEqIdx=0; energyEqIdx< numEnergyEqs; ++energyEqIdx)
//					values.setDirichlet(energyEq0Idx + energyEqIdx);
//
//            values.setDirichlet(p0Idx);
//            values.setDirichlet(S0Idx);

        }


//        // No-Flow for wPhase
//        values.setNeumann(conti00EqIdx + wPhaseIdx*numComponents + wCompIdx) ;
//        values.setNeumann(conti00EqIdx + wPhaseIdx*numComponents + nCompIdx) ;
//        values.setNeumann(energyEq0Idx + wPhaseIdx ) ;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * \param values The dirichlet values for the primary variables
     * \param vertex The vertex for which the boundary type is set
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichlet(PrimaryVariables &values, const Vertex &vertex) const
    {
        const GlobalPosition & globalPos = vertex.geometry().center();
    	initial_(values, globalPos);
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each component. Negative values mean
     * influx.
     */
    void neumann(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const Intersection &is,
                 const unsigned int scvIdx,
                 const unsigned int boundaryFaceIdx) const
    {
        // default: neumann no-flow
        values = 0;
        GlobalPosition globalPos = fvGeometry.subContVol[scvIdx].global ;

		// equilibrium composition
		Valgrind::CheckDefined(TInitial_);
		Valgrind::CheckDefined(pnInitial_);

		const Scalar depth = 100e-6 ; // 100 microns depth
		const Scalar width = 0.0144; /*height of micromodel: 14.4mm*/
		const Scalar crossSectionalArea = width * depth;

		ParameterCache dummyCache;
		FluidState fluidState;

		for(int phaseIdx=0; phaseIdx<numPhases; phaseIdx++)
			fluidState.setPressure(phaseIdx, pnInitial_);

		if(numEnergyEquations){
			fluidState.setTemperature(nPhaseIdx, TInitial_ );
			fluidState.setTemperature(wPhaseIdx, TInitial_ ); // this value is a good one, TInject does not work
		}
		else
			fluidState.setTemperature(TInject_ );


		// This solves the system of equations defining x=x(p,T)
		FluidSystem::calculateEquilibriumMoleFractions(fluidState, dummyCache) ;
		// compute density of injection phase
		const Scalar density = FluidSystem::density(fluidState,
													dummyCache,
													injectionPhaseIdx_);

		fluidState.setDensity(injectionPhaseIdx_, density);
		const Scalar avMolMass = fluidState.averageMolarMass(injectionPhaseIdx_) ;

		const Scalar areaSpecificMolarFlux = massFluxInjectedPhase_ / avMolMass / crossSectionalArea; // ;//
		const Scalar areaSpecificMassFlux = massFluxInjectedPhase_  / crossSectionalArea ; // ; //

		if(enableKinetic){
			values[conti00EqIdx + injectionPhaseIdx_ * numComponents + wCompIdx] = areaSpecificMolarFlux * fluidState.moleFraction(injectionPhaseIdx_,wCompIdx);
			values[conti00EqIdx + injectionPhaseIdx_ * numComponents + nCompIdx] = areaSpecificMolarFlux * fluidState.moleFraction(injectionPhaseIdx_,nCompIdx);
		}
		else{
			values[conti00EqIdx+ wCompIdx] = areaSpecificMolarFlux * fluidState.moleFraction(wPhaseIdx, wCompIdx);
			values[conti00EqIdx+ nCompIdx] = areaSpecificMolarFlux * fluidState.moleFraction(wPhaseIdx, nCompIdx);
		}

		 for(int phaseIdx=0; phaseIdx<numPhases; ++phaseIdx){
			 const Scalar h = FluidSystem::enthalpy(fluidState,
											  dummyCache,
											  phaseIdx);
			 fluidState.setEnthalpy(phaseIdx, h);
		 }

		 // energy equations are specified mass specifically
		 if(numEnergyEquations){
			 values[energyEq0Idx + injectionPhaseIdx_] = areaSpecificMassFlux
														 * fluidState.enthalpy(injectionPhaseIdx_) ;
		 }
		 else if(enableEnergy)
			 values[energyEq0Idx] = areaSpecificMassFlux
									 * fluidState.enthalpy(injectionPhaseIdx_) ;
    }
    // \}



    /*!
     * \name Volume terms
     */
    // \{

    /*!
    * \brief Evaluate the source term for all phases within a given
    * sub-control-volume.
    *
    * This is the method for the case where the source term is
    * potentially solution dependent and requires some quantities that
    * are specific to the fully-implicit method.
    *
    * \param values The source and sink values for the conservation equations in
    units of
    * \f$ [ \textnormal{unit of conserved quantity} / (m^3 \cdot s
    )] \f$
    * \param element The finite element
    * \param fvGeometry The finite-volume geometry
    * \param scvIdx The local subcontrolvolume index
    * \param elemVolVars All volume variables for the element
    *
    * For this method, the \a values parameter stores the rate mass
    * generated or annihilate per volume unit. Positive values mean
    * that mass is created, negative ones mean that it vanishes.
    */
    void solDependentSource(PrimaryVariables & priVars,
    const Element &element,
    const FVElementGeometry &fvGeometry,
    const int scvIdx,
    const ElementVolumeVariables &elemVolVars) const
    {
		priVars = Scalar(0.0);

		// due to 1d Model: volume of element is length
		const Scalar length = fvGeometry.subContVol[scvIdx].volume;
		const Scalar width = 0.0144; /*height of micromodel: 14.4mm*/
		// this is the "area" (if it was in 3d) over which energy is lost to environment.
		const Scalar area = length * width ;

        const Scalar extrusionFactor =
            elemVolVars[scvIdx].extrusionFactor();
        const Scalar volume = fvGeometry.subContVol[scvIdx].volume * extrusionFactor; // volume is without extrusion, i.e. the length Bernd: correct

		const Scalar currentTemp = elemVolVars[scvIdx].temperature(sPhaseIdx);
		const Scalar deltaT = currentTemp - TAmb_ ;

        // make the absolute source a volume specific one
		priVars[energyEq0Idx + sPhaseIdx] = - area * deltaT * heatTransferCoeff_  / volume ;
    }


    /*!
      * \brief Return how much the domain is extruded at a given sub-control volume.
      *
      * This means the factor by which a lower-dimensional (1D or 2D)
      * entity needs to be expanded to get a full dimensional cell. The
      * default is 1.0 which means that 1D problems are actually
      * thought as pipes with a cross section of 1 m^2 and 2D problems
      * are assumed to extend 1 m to the back.
      */
     Scalar boxExtrusionFactor(const Element &element,
                               const FVElementGeometry &fvGeometry,
                               const int scvIdx) const
     {
         return 1.4e-6; // 14.4mm * 100microns
     }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initial(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const unsigned int scvIdx) const
    {
        const GlobalPosition &globalPos = element.geometry().corner(scvIdx);
        initial_(values, globalPos);
    }

    // \}

private:
    // the internal method for the initial condition
    void initial_(PrimaryVariables & values,
                  const GlobalPosition & globalPos) const
    {
        Scalar Temperature = TInitial_;
        Scalar S[numPhases];
        Scalar p[numPhases];


        S[wPhaseIdx]    = SWaterInitial_; // 1e-8; //

        // on the left boundary there is warm (non-wetting) water
        if ( onLeftBoundary_(globalPos) ){
        	S[wPhaseIdx] = SWaterBoundary_;
            Temperature = TInject_ ;
        }
        S[nPhaseIdx]    = 1. - S[wPhaseIdx] ;

        // nPhaseIdx: here: non-waterIdx, water is non-wetting, invading, gets pc on top
        p[nPhaseIdx]    = pnInitial_ ; // - 853. / this->bBoxMax()[0] * globalPos[0] ;  //

        // capillary pressure parameters
        FluidState fluidState;
        const MaterialLawParams &materialParams =
            this->spatialParams().materialLawParams();
        Scalar capPress[numPhases];

        //set saturation to inital values, this needs to be done in order for the fluidState to tell me pc
        for (int phaseIdx = 0; phaseIdx < numPhases ; ++phaseIdx) {
            fluidState.setSaturation(phaseIdx, S[phaseIdx]);
            if (enableEnergy){
                if(numEnergyEquations)
                    fluidState.setTemperature(phaseIdx,Temperature );
                else
                    fluidState.setTemperature(Temperature );
            }
        }

        //obtain pc according to saturation
        MaterialLaw::capillaryPressures(capPress, materialParams, fluidState);
        // set pressures from capillary pressures
        p[wPhaseIdx] = p[nPhaseIdx] + capPress[wPhaseIdx] ;

        for(int phaseIdx=0; phaseIdx<numPhases; phaseIdx++)
              fluidState.setPressure(phaseIdx, p[phaseIdx]);

        // This solves the system of equations defining x=x(p,T)
        ParameterCache dummyCache;
        FluidSystem::calculateEquilibriumMoleFractions(fluidState, dummyCache) ;

        /* Difference between kinetic and MPNC:
         * number of component related primVar and how they are calculated (mole fraction, fugacities, resp.)
         */
        static_assert(( (enableKinetic==true)  ),
                      "This only works for the kinetic considerations."
                      "Although the phases are pure, there are no relations for chemical potentials available"
                      "I.e. no Henry, vapor pressure.");

		for(int phaseIdx=0; phaseIdx < numPhases; ++ phaseIdx)
		{
			for(int compIdx=0; compIdx <numComponents; ++compIdx){
				int offset = compIdx + phaseIdx * numComponents  ;
				values[conti00EqIdx + offset] = fluidState.moleFraction(phaseIdx,compIdx) ;
			}
		}

		/////////////////////
		// Actually setting all the primary variables
		////////////////////

        // temperature
        if(enableEnergy or numEnergyEquations)
            for (int energyEqIdx=0; energyEqIdx< numEnergyEqs; ++energyEqIdx)
                values[energyEq0Idx + energyEqIdx] = Temperature ;

        values[S0Idx] = S[wPhaseIdx] ;

        if(pressureFormulation == mostWettingFirst){
            values[p0Idx] = p[nPhaseIdx];
        }
        else if(pressureFormulation == leastWettingFirst){
            values[p0Idx] = p[wPhaseIdx];
        }
        else DUNE_THROW(Dune::InvalidStateException, "Formulation: " << pressureFormulation << " is invalid.");

    }

    /*!
     * \brief Give back whether the testes position (input) is a specific region (left) in the domain
     */
    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    {       return globalPos[0] < (this->bBoxMin()[0] + eps_ ) ;   }

    /*!
     * \brief Give back whether the tested position (input) is a specific region (right) in the domain
     */
    bool onRightBoundary_(const GlobalPosition & globalPos) const
    {        return globalPos[0] > this->bBoxMax()[0] - eps_;    }

private:
    Scalar eps_;
    std::string outputName_;
    int nRestart_;
    Scalar massFluxInjectedPhase_;
    Scalar TInitial_ ;
    Scalar SWaterInitial_ ;
    Scalar SWaterBoundary_ ;
    Scalar pnInitial_;
    Dune::ParameterTree inputParameters_;
    Scalar xInject_ ;
    Scalar TInject_;
    int injectionPhaseIdx_;
    Scalar x_[numPhases][numComponents] ;
    Scalar TAmb_ ;
    Scalar heatTransferCoeff_ ;


    PhaseDimVectorField  volumeDarcyVelocity_;
    PhaseVector         volumeDarcyMagVelocity_ ;
    ScalarVector        boxSurface_;

    Dune::ParameterTree getInputParameters() const
    { return inputParameters_; }
};

} //end namespace

#endif
