/*****************************************************************************
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_KINETICSOIL_HH
#define DUMUX_KINETICSOIL_HH

#include <dumux/material/spatialparams/implicitspatialparams.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>
#include <dumux/material/fluidmatrixinteractions/2p/vangenuchtenoftemperature.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchtenkrpcdifferentparams.hh>
#include <dumux/material/fluidmatrixinteractions/mp/2padapter.hh>
#include <dumux/material/fluidmatrixinteractions/mp/2poftadapter.hh>
#include <dumux/material/fluidmatrixinteractions/2p/philtophoblaw.hh>


// material laws for interfacial area
#include <dumux/material/fluidmatrixinteractions/2pia/efftoabslawia.hh>
#include <dumux/material/fluidmatrixinteractions/2pia/philtophoblawia.hh>//
#include <dumux/material/fluidmatrixinteractions/2pia/awnsurfacepolynomial2ndorder.hh>
#include <dumux/material/fluidmatrixinteractions/2pia/linearInterfacialArea.hh>
#include <dumux/material/fluidmatrixinteractions/2pia/linearThroughOriginInterfacialArea.hh>

#include <dumux/material/fluidmatrixinteractions/2pia/awnsurfacepolynomialedgezero2ndorder.hh>
#include <dumux/material/fluidmatrixinteractions/2pia/awnsurfaceexpfct.hh>
#include <dumux/material/fluidmatrixinteractions/2pia/awnsurfacepcmaxfct.hh>
#include <dumux/material/fluidmatrixinteractions/2pia/awnsurfaceexpswpcto3.hh>
//#include <dumux/material/fluidmatrixinteractions/2pia/awnsurfacesaturationto2.hh>
//#include <appl/heatpipe/heatpipelaw.hh>

namespace Dumux
{

//forward declaration
template<class TypeTag>
class MicroModelSpatialParams;

namespace Properties
{
// The spatial params TypeTag
NEW_TYPE_TAG(MicroModelSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(MicroModelSpatialParams, SpatialParams, Dumux::MicroModelSpatialParams<TypeTag>);

// Set the material Law
SET_PROP(MicroModelSpatialParams, MaterialLaw)
{
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

    enum {wPhaseIdx   = FluidSystem::wPhaseIdx};

private:
    // define the material law which is parameterized by effective
    // saturations
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    // define the material law
//    typedef RegularizedVanGenuchten<Scalar> EffectiveLaw;
    typedef RegularizedVanGenuchtenDifferentKrPc<Scalar> EffectiveLaw;

//        typedef EffToAbsLaw<EffectiveLaw>       TwoPMaterialLaw;
        typedef PhilToPhobLaw<EffectiveLaw>       TwoPMaterialLaw;
    public:

//        typedef TwoPOfTAdapter<wPhaseIdx, TwoPMaterialLaw> type;
        typedef TwoPAdapter<wPhaseIdx, TwoPMaterialLaw > type;
};


// Set the interfacial area relation: wetting -- non-wetting
SET_PROP(MicroModelSpatialParams, AwnSurface)
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLawParams) MaterialLawParams;
    typedef LinearInterfacialArea<Scalar> EffectiveIALaw;
public:
//    typedef EffToAbsLawIA<EffectiveIALaw, MaterialLawParams> type;
    typedef PhilToPhobLawIA<EffectiveIALaw, MaterialLawParams> type;
};


// Set the interfacial area relation: wetting -- solid
SET_PROP(MicroModelSpatialParams, AwsSurface)
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLawParams) MaterialLawParams;
    typedef LinearInterfacialAreaThroughOrigin<Scalar>                  EffectiveIALaw;
public:
//    typedef EffToAbsLawIA<EffectiveIALaw, MaterialLawParams> type;
    typedef PhilToPhobLawIA<EffectiveIALaw, MaterialLawParams> type;
};

// Set the interfacial area relation: non-wetting -- solid
SET_PROP(MicroModelSpatialParams, AnsSurface)
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLawParams) MaterialLawParams;
    typedef LinearInterfacialArea<Scalar>      EffectiveIALaw;
public:
//    typedef EffToAbsLawIA<EffectiveIALaw, MaterialLawParams> type;
    typedef PhilToPhobLawIA<EffectiveIALaw, MaterialLawParams> type;
};


} // end namespace properties




/**
 * \brief Definition of the soil properties for the kinetic Energy problem
 *
 */
template<class TypeTag>
class MicroModelSpatialParams : public ImplicitSpatialParams<TypeTag>
{
    typedef ImplicitSpatialParams<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GridView::ctype CoordScalar;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

    enum {dim=GridView::dimension };
    enum {dimWorld=GridView::dimensionworld};
    enum {wPhaseIdx = FluidSystem::wPhaseIdx};
    enum {nPhaseIdx = FluidSystem::nPhaseIdx};
    enum {sPhaseIdx = FluidSystem::sPhaseIdx};

    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GridView::template Codim<0>::Entity Element;
    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;
    typedef Dune::FieldVector<CoordScalar,dimWorld> DimVector;

    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;


public:

    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;

    typedef typename GET_PROP_TYPE(TypeTag, AwnSurface) AwnSurface;
    typedef typename AwnSurface::Params AwnSurfaceParams;

    typedef typename GET_PROP_TYPE(TypeTag, AwsSurface) AwsSurface;
    typedef typename AwsSurface::Params AwsSurfaceParams;

    typedef typename GET_PROP_TYPE(TypeTag, AnsSurface) AnsSurface;
    typedef typename AnsSurface::Params AnsSurfaceParams;

    MicroModelSpatialParams(const GridView &gv)
        : ParentType(gv)
    {
    }

    ~MicroModelSpatialParams()
    {}

    //! load parameters from input file and initialize parameter values
    void setInputInitialize()
    {

        // BEWARE! First the input values have to be set, than the material parameters can be set

        // this is the parameter value from file part
        try{
        intrinsicPermeability_      = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.permeability);
        porosity_                   = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.porosity);
        densitySolid_                		= GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.soil.density);
        thermalConductivitySolid_    		= GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.soil.soilThermalConductivity);
        heatCapacity_               = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.soil.heatCapacity);

        materialLawFirstParam_      = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.soil.VGAlpha ); //  // SpatialParams.soil.LinearFirst
        materialLawSecondParam_     = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.soil.VGN); // SpatialParams.soil.LinearSecond

        VGkrN_     					= GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.soil.VGkrN); // SpatialParams.soil.LinearSecond


        characteristicLength_       = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.soil.meanPoreSize);

        factorEnergyTransfer_           = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.factorEnergyTransfer);
        factorMassTransfer_             = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.factorMassTransfer);

        aWettingNonWettingA1_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.soil.aWettingNonWettingA1);

        aNonWettingSolidA1_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.soil.aNonWettingSolidA1);

        aWettingSolidA1_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.soil.aWettingSolidA1);

        Swr_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.soil.Swr);
        Snr_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.soil.Snr);

        }

        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown!\n";
            exit(1);
        }

        // residual saturations
        materialParams_.setSwr(Swr_);
        materialParams_.setSnr(Snr_);

        materialParams_.setVgAlpha(materialLawFirstParam_);
        materialParams_.setVgn(materialLawSecondParam_);

        materialParams_.setVgkrn(VGkrN_	);

        // wetting-non wetting: surface which goes to zero on the edges, but is a polynomial
        aWettingNonWettingSurfaceParams_.setA1(aWettingNonWettingA1_);

        // non-wetting-solid
        aNonWettingSolidSurfaceParams_.setA1(aNonWettingSolidA1_);

        // wetting-solid
        aWettingSolidSurfaceParams_.setA1(aWettingSolidA1_);
    }

    /*!
     * \brief Update the spatial parameters with the flow solution
     *        after a timestep.
     *
     * \TODO call parameters
     */
    void update(const SolutionVector &globalSolutionFn)
    {
    }

    /*!\brief Return the maximum capillary pressure for the given pc-Sw curve
     *
     *        Of course physically there is no such thing as a maximum capillary pressure.
     *        The parametrization (VG/BC) goes to infinity and physically there is only one pressure
     *        for single phase conditions.
     *        Here, this is used for fitting the interfacial area surface: the capillary pressure,
     *        where the interfacial area is zero.
     *        Technically this value is obtained as the capillary pressure of saturation zero.
     *        This value of course only exists for the case of a regularized pc-Sw relation.
     * \param element     The finite element
     * \param fvGeometry  The finite volume geometry
     * \param scvIdx      The local index of the sub-control volume */
    const Scalar pcMax(const Element & element,
                       const FVElementGeometry & fvGeometry,
                       const unsigned int scvIdx) const
    { return aWettingNonWettingSurfaceParams_.pcMax() ; }


    const AwnSurfaceParams & aWettingNonWettingSurfaceParams(const Element &element,
                                                             const FVElementGeometry &fvGeometry,
                                                             const unsigned int scvIdx) const
    { return aWettingNonWettingSurfaceParams_ ; }
    const AwnSurfaceParams & aWettingNonWettingSurfaceParams() const
    { return aWettingNonWettingSurfaceParams_ ; }

    const AwsSurfaceParams & aWettingSolidSurfaceParams(const Element &element,
                                                        const FVElementGeometry &fvGeometry,
                                                        const unsigned int scvIdx) const
    { return aWettingSolidSurfaceParams_ ; }
    const AwsSurfaceParams & aWettingSolidSurfaceParams() const
    { return aWettingSolidSurfaceParams_ ; }

    const AnsSurfaceParams & aNonWettingSolidSurfaceParams(const Element &element,
                                                           const FVElementGeometry &fvGeometry,
                                                           const unsigned int scvIdx) const
    { return aNonWettingSolidSurfaceParams_ ; }
    const AnsSurfaceParams & aNonWettingSolidSurfaceParams() const
    { return aNonWettingSolidSurfaceParams_ ; }

    /*!
     * \brief Returns the intrinsic permeability for a sub control volume
     *
     * \param element The current finite element
     * \param fvElemGeom The current finite volume geometry of the element
     * \param scvfIdx The index sub-control volume face where the
     *                      intrinsic velocity ought to be calculated.
     */
    Scalar intrinsicPermeability(const Element &element,
                                 const FVElementGeometry &fvGeometry,
                                 const unsigned int faceIdx) const
    { return intrinsicPermeability_; }

    /*!
     * \brief Define the porosity \f$[-]\f$ of the soil
     *
     * \param vDat The data defined on the sub-control volume
     * \param element The finite element
     * \param fvElemGeom The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     *                    the porosity needs to be defined
     */
    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvElemGeom,
                    const unsigned int scvIdx) const
    {
        return porosity_;
    }

    /*!
     * \brief Return a reference to the material parameters of the material law.
     */
    const MaterialLawParams& materialLawParams(const Element &element,
                                               const FVElementGeometry &fvGeometry,
                                               const unsigned int scvIdx) const
    {return materialParams_;}
    const MaterialLawParams& materialLawParams() const
    {return materialParams_;}

    /*!
     * \brief Returns the heat capacity \f$[J/m^3 K]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param element     The finite element
     * \param fvElemGeom  The finite volume geometry
     * \param scvIdx      The local index of the sub-control volume where
     *                    the heat capacity needs to be defined
     */
    const Scalar  heatCapacity(const Element &element,
                               const FVElementGeometry &fvGeometry,
                               const unsigned int scvIdx) const
    { return heatCapacity_ ;  } // specific heat capacity of porous material [J / (kg K)]


    /*!\brief Returns the density \f$[kg/m^3]\f$ of the rock matrix.
     * \param element     The finite element
     * \param fvGeometry  The finite volume geometry
     * \param scvIdx      The local index of the sub-control volume */
    const Scalar densitySolid(const Element & element,
                              const FVElementGeometry & fvGeometry,
                              const unsigned int scvIdx) const
    { return densitySolid_ ;} // density of solid [kg/m^3]


    /*!\brief Returns the thermal conductivity \f$[W/(m K)]\f$ of the rock matrix.
     * \param element     The finite element
     * \param fvGeometry  The finite volume geometry
     * \param scvIdx      The local index of the sub-control volume */
    const Scalar  thermalConductivitySolid(const Element &element,
                                    const FVElementGeometry &fvGeometry,
                                    const unsigned int scvIdx)const
    { return thermalConductivitySolid_ ; } // conductivity of solid  [W / (m K ) ]

    /*!\brief Return the characteristic length for the mass transfer.
     *
     *        The position is determined based on the coordinate of
     *        the vertex belonging to the considered sub controle volume.
     * \param element     The finite element
     * \param fvGeometry  The finite volume geometry
     * \param scvIdx      The local index of the sub control volume */
    const Scalar characteristicLength(const Element & element,
                                      const FVElementGeometry & fvGeometry,
                                      const unsigned int scvIdx) const
    {   const  GlobalPosition & globalPos =  fvGeometry.subContVol[scvIdx].global ;
        return characteristicLengthAtPos(globalPos); }
    /*!\brief Return the characteristic length for the mass transfer.
     * \param globalPos The position in global coordinates.*/
    const Scalar characteristicLengthAtPos(const  GlobalPosition & globalPos) const
    {            return characteristicLength_ ;    }

    /*!\brief Return the pre factor the the energy transfer
     *
     *        The position is determined based on the coordinate of
     *        the vertex belonging to the considered sub controle volume.
     * \param element     The finite element
     * \param fvGeometry  The finite volume geometry
     * \param scvIdx      The local index of the sub control volume */
    const Scalar factorEnergyTransfer(const Element & element,
                                      const FVElementGeometry & fvGeometry,
                                      const unsigned int scvIdx) const
    {   const  GlobalPosition & globalPos =  fvGeometry.subContVol[scvIdx].global ;
        return factorEnergyTransferAtPos(globalPos); }
    /*!\brief Return the pre factor the the energy transfer
     * \param globalPos The position in global coordinates.*/
    const Scalar factorEnergyTransferAtPos(const GlobalPosition & globalPos) const
    {            return factorEnergyTransfer_ ;    }

    /*!\brief Return the pre factor for the mass transfer
     *
     *        The position is determined based on the coordinate of
     *        the vertex belonging to the considered sub controle volume.
     * \param element     The finite element
     * \param fvGeometry  The finite volume geometry
     * \param scvIdx      The local index of the sub control volume */
    const Scalar factorMassTransfer(const Element & element,
                                    const FVElementGeometry & fvGeometry,
                                    const unsigned int scvIdx) const
    {   const  GlobalPosition & globalPos =  fvGeometry.subContVol[scvIdx].global ;
        return factorMassTransferAtPos(globalPos); }
    /*!\brief Return the pre factor the the mass transfer
     * \param globalPos The position in global coordinates.*/
    const Scalar factorMassTransferAtPos(const GlobalPosition & globalPos) const
    {            return factorMassTransfer_ ;    }


private:
    MaterialLawParams 	materialParams_ ;
    AwnSurfaceParams    aWettingNonWettingSurfaceParams_ ;
    AwsSurfaceParams 	aWettingSolidSurfaceParams_ ;
    AnsSurfaceParams 	aNonWettingSolidSurfaceParams_ ;
    Scalar 				intrinsicPermeability_ ;
    Scalar 				porosity_ ;
    Scalar 				densitySolid_ ;
    Scalar 				thermalConductivitySolid_ ;
    Scalar 				heatCapacity_ ;
    Scalar              materialLawFirstParam_  ;
    Scalar              materialLawSecondParam_ ;
    Scalar 				VGkrN_ ;

    Scalar characteristicLength_ ;

    Scalar factorEnergyTransfer_ ;
    Scalar factorMassTransfer_ ;

    // interfacial area parameters
    Scalar aWettingNonWettingA1_ ;

    Scalar aNonWettingSolidA1_;

    Scalar aWettingSolidA1_;


    Scalar Swr_;
    Scalar Snr_;
};

}

#endif // GUARDIAN
